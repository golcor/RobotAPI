/**
\page RobotAPI-HowTos RobotAPI HowTos
\brief &nbsp;




\section RobotAPI-HowTos-Eigen-From-Pose How to convert a pose variants to an Eigen matrices

Use the following code if you want to convert from an armarx::Pose, an armarx::FramedPose or an armarx::LinkedPose.
Refer to \ref RobotAPI-FramedPose for further information.

\code
    #include <RobotAPI/libraries/core/LinkedPose.h>

    std::string refFrame = "Base";

    // If you are in a statechart
    FramedPosePtr pose = getInput<FramedPose>("pose");

    Eigen::Matrix4f matrix = pose->toEigen();
\endcode





\section RobotAPI-HowTos-Eigen-From-PosOrient How to convert position and orientation variants to an Eigen matrices

Use the following code if you want to convert from armarx::FramedPosition and armarx::FramedOrientation.
Refer to \ref RobotAPI-FramedPose for further information.

\code
    #include <RobotAPI/libraries/core/LinkedPose.h>

    std::string refFrame = "Base";

    // If you are in a statechart
    FramedPositionPtr position = getInput<FramedPosition>("position");
    FramedOrientationPtr orientation = getInput<FramedOrientation>("orientation");

    PosePtr pose = new Pose(position, orientation);

    Eigen::Matrix4f matrix = pose->toEigen();
\endcode




\section RobotAPI-HowTos-Change-Pose-Frame How to change a frame of a coordinate/pose

Refer to \ref RobotAPI-FramedPose.




\section RobotAPI-HowTos-RemoteRobot-Start How to start a RobotStateComponent

The RobotState component serves as a central component for storing all robot related data. For now this data covers the current joint angles of the robot.<br>
The RobotStateComponent implements a KinematicUnitListener, hence it reacts on all joint updates that are reported by a KinematicUnit component. An exemplary startup script could look like this

\code
    export CORE_PATH=${ArmarXHome_DIR}/Core
    export CORE_BIN_PATH=$CORE_PATH/build/bin
    export SCRIPT_PATH=$CORE_BIN_PATH

    $SCRIPT_PATH/startApplication.sh $CORE_BIN_PATH/KinematicUnitSimulationRun --Ice.Config=./config/Armar3Config.cfg &
    $SCRIPT_PATH/startApplication.sh $CORE_BIN_PATH/RobotStateComponentRun --Ice.Config=./config/Armar3Config.cfg &
\endcode

With the corresponding configuration in ./config/Armar3Config.cfg:

\code
    # setup for KinemticUnitSimulation
    ArmarX.KinematicUnitSimulation.RobotFileName = RobotAPI/robots/Armar3/ArmarIII.xml
    ArmarX.KinematicUnitSimulation.RobotNodeSetName = Robot
    ArmarX.KinematicUnitSimulation.ObjectName = Armar3KinematicUnit

    #setup for RobotStateComponent
    ArmarX.RobotStateComponent.RobotFileName = RobotAPI/robots/Armar3/ArmarIII.xml
    ArmarX.RobotStateComponent.RobotNodeSetName = Robot
    ArmarX.RobotStateComponent.ObjectName = RobotStateComponent
\endcode


\section RobotAPI-HowTos-RobotViewer How to inspect the robot's structure 

Robots are usually defined in the Simox XML (https://gitlab.com/Simox/simox/wikis/FileFormat) or in the URDF format. To inspect the kinematic structure, visualizations and physical properties, you can use the <i>RobotViewer</i> tool which is part of the Simox library. In particlular you can visualize all coordinate frames that are present in the robot defintion.
Start it with the following command:
\code
    RobotViewer --robot path/to/robot.xml
\endcode

\image html robotviewer.png "The Simox tool RobotViewer can be used to inspect the kinematic structure of a robot."

\section RobotAPI-HowTos-RemoteRobot-Access How to access a RobotStateComponent

The RobotStateComponent provides several methods for accessing the current configuration of the robot and for getting a snapshot of the current state which is compatible with
models of the Simox/VirtualRobot framework. With these models the whole functionality of Simox (https://gitlab.com/Simox/simox) can be used, e.g. IK solving, collision detection or motion and grasp planning.

\par Creating a synchronized model (RemoteRobot)

A RemoteRobot is a synchronized robot data structure which always represents the current state of the robot.
Be aware, that any operations on this model (e.g. IK solving) may take long (e.g. 100 ms) due to the heavy network communication overhead.
For complex operations it is suggested to create a local clone of the data structure and to synchronize this clone before working with it (see below).

The Remote Robot can be created by getting the proxy to the RobotStateComponent and grabbing a RemoteRobot:

\code
    std::string robotStateComponentName = "RobotState";
    armarx::RobotStateComponentInterfacePrx robotStateComponent = getProxy<RobotStateComponentInterfacePrx>(robotStateComponentName);
    armarx::RemoteRobotPtr remoteRobot(new RemoteRobot(robotStateComponent->getSynchronizedRobot()));
\endcode

The remoteRobot object can now be accessed in order to get joint angles or to convert cooridnate frames.

\par Create a local robot and synchronize it by hand

When complex operations should be performed on a robot model the use of a RemoteRobot could slow down the computation since each joint access induces a network transfer.
Hence the RemoteRobot offers a method to create a local copy of the remote data.<br>
If only the structure of the robot is needed (without 3D models, useful e.g. for kinematic calculations, coorinate transformations, etc),
the following method can be used to create a local clone of the robot:
\code
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalClone(robotStateComponent);
\endcode
The robot instance can be manually synchronized with the remote data structure (i.e. copy the joint angle values) by calling:
\code
    RemoteRobot::synchronizeLocalClone(robot,robotStateComponent);
\endcode

If a complete robot model (including 3d models) is needed, you can pass a filename to a local file to allow the system to load the complete robot:
\code
    std::string robotFile = "RobotAPI/robots/Armar3/ArmarIII.xml";
    ArmarXDataPath::getAbsolutePath(robotFile,robotFile);
    VirtualRobot::RobotPtr robot = RemoteRobot::createLocalClone(robotStateComponent, filename);
\endcode

This model can be synchronized in the same way as the first model.
*/
