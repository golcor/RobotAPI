/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    RemoteRobotTestProject::
* @author     ( at kit dot edu)
* @date       
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RemoteRobotTestProject.h"

#include <Core/robotstate/RobotStateObjectFactories.h>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/LinkedCoordinate.h>

namespace armarx
{
    void RemoteRobotTestProject::onInitComponent()
    {
        std::cout << "onInitComponent" << std::endl;
        

	usingProxy("RobotStateComponent");
	
	
	 

        // onInitComponent has to do the following tasks
        // 1) Make sure the component is in a consistent state (initialize all members).
        //   After onInitComponent, the Ice object adapter is activated and the component
        //   is accessible via Ice. Therefore, the component needs to be in a sane state.
        // 2) Make Ice dependencies explicit
        //   All proxies and topics used by this component need to be made explicit. Further
        //   all topics that are provided by the component need to be made explicit. 
        //   Use the following calls:
        //       usingProxy("proxyName"): this component used the proxy "proxyName"
        //       usingTopic("topciName"): this component subscribes to the topic "topicName"
        //       offeringTopic("topciName"): this component provides the topic "topicName"
        //   The initialization procedure assures that all required proxies and topics are
        //   available before the component is started.
        
    }
    void test2(VirtualRobot::RobotPtr robot){
	    std::cout << "!!!!!!!!!!!Robot: " << robot->getName() << std::endl;
    }
	
    void test(VirtualRobot::RobotWeakPtr robot){
	    //std::cout << "!!!!!!Robot: " << robot->getName() << std::endl;
	    test2(robot.lock());
    }
	

    /* Comment for gdb:
     * start with: startApplication -> debugApplication (w/o `&' at the end)
     * gdb $> b armarx::RemoteRobotTestProject::onStartComponent()
     * gdb $> b RobotNodeSet.cpp:46
     * gdb $> b RemoteRobot.cpp:48    
     */
    void RemoteRobotTestProject::onStartComponent()
    {
        std::cout << "onStartComponent" << std::endl;
	RobotStateObjectFactories::addFactories(getIceManager()->ic());
	
	this->server = getProxy<RobotStateComponentInterfacePrx>("RobotStateComponent");
	this->robot.reset(new RemoteRobot(server->getSynchronizedRobot()));
   	
	// method removed 
	//test(boost::dynamic_pointer_cast<RemoteRobot>(robot)->getRobotPtr());

	VirtualRobot::RobotNodeSetPtr set = this->robot->getRobotNodeSet("LeftArm");	

    	VirtualRobot::RobotNodePtr robotNodePtr = this->robot->getRobotNode("Wrist 2 L");
	
	VirtualRobot::LinkedCoordinate c(this->robot);
	c.set(robotNodePtr);
	c.changeFrame("Shoulder 1 L");
	
	std::cout << "Left tcp in right tcp frame: " << c.getPosition() << std::endl;
	

        ARMARX_LOG << eINFO << "Starting RemoteRobotTestProject" << flush;
        
        // onStartComponent is called once after the initialization procedure finished and the Ice object adapter is activated.
        // common tasks are:
        // 1) retrieve used topic:
        //    MyTopicPrx topicProxy = getTopic<MyTopicPrx>("topicName");
        // 2) retrieve used proxy:
        //    MyPrx proxy = getProxy<MyPrx>("proxyName");
        
    }
}
