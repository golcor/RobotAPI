set(SCENARIO_CONFIG_COMPONENTS
    config/RobotControl.cfg
    config/XMLRemoteStateOfferer.StatechartProfilesTestGroup.cfg
	config/SystemObserver.cfg
	config/ConditionHandler.cfg


        #WeissHapticUnitApp
        #WeissHapticSensorApp
        #HapticObserverApp
)


# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario_from_configs("StatechartProfilesTest" "${SCENARIO_CONFIG_COMPONENTS}")
