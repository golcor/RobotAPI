
armarx_component_set_name(TCPControlUnit)

find_package(Eigen3 QUIET)
armarx_build_if(Eigen3_FOUND "Eigen3 not available")

if (Eigen3_FOUND)
    include_directories(
        ${Eigen3_INCLUDE_DIR})
endif()

if (NOT Simox_FOUND)
    find_package(Simox ${ArmarX_Simox_VERSION} QUIET)
endif()
armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")
include_directories(${Simox_INCLUDE_DIRS})

set(COMPONENT_LIBS RobotAPIUnits)

set(SOURCES main.cpp TCPControlUnitApp.h)

armarx_add_component_executable("${SOURCES}")
