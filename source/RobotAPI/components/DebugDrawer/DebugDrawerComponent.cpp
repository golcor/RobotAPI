/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::ArmarXObjects::DebugDrawerComponent
 * @author     Nikolaus Vahrenkamp ( vahrenkamp at kit dot edu )
 * @author     Peter Kaiser ( peter dot kaiser at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DebugDrawerComponent.h"


#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <Inventor/nodes/SoUnits.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoMaterial.h>
#include <Inventor/nodes/SoAnnotation.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoFont.h>
#include <Inventor/nodes/SoText2.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoCylinder.h>
#include <Inventor/nodes/SoComplexity.h>
#include <Inventor/nodes/SoCoordinate3.h>
#include <Inventor/nodes/SoPointSet.h>
#include <Inventor/actions/SoWriteAction.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/nodes/SoMaterialBinding.h>
#include <Inventor/nodes/SoDrawStyle.h>
#include <Inventor/nodes/SoLineSet.h>
#include <Inventor/SbVec3f.h>
#include <Inventor/fields/SoMFVec3f.h>
#include <Inventor/fields/SoMFColor.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>

#include <VirtualRobot/Robot.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualizationFactory.h>

using namespace VirtualRobot;

#define SELECTION_NAME_PREFIX   ("selection_" + boost::lexical_cast<std::string>(this) + "_")

namespace armarx
{
    boost::recursive_mutex DebugDrawerComponent::selectionMutex;

    void selection_callback(void* userdata, SoPath* path)
    {
        if (userdata)
        {
            ((DebugDrawerComponent*) userdata)->selectionCallback();
        }
    }

    void deselection_callback(void* userdata, SoPath* path)
    {
        if (userdata)
        {
            ((DebugDrawerComponent*) userdata)->deselectionCallback();
        }
    }

    static const std::string DEBUG_LAYER_NAME
    {"debug"
    };

    DebugDrawerComponent::DebugDrawerComponent():
        mutex(new RecursiveMutex()),
        dataUpdateMutex(new RecursiveMutex()),
        topicMutex(new RecursiveMutex())
    {
        cycleTimeMS = 1000.0f / 30.0f;
        timerSensor = NULL;
        verbose = true;

        coinVisu = new SoSeparator;
        coinVisu->ref();

        selectionNode = new SoSelection;
        selectionNode->policy = SoSelection::SHIFT;
        coinVisu->addChild(selectionNode);

        layerMainNode = new SoSeparator;
        layerMainNode->ref();

        SoUnits* u = new SoUnits();
        u->units = SoUnits::MILLIMETERS;
        selectionNode->addChild(u);
        selectionNode->addChild(layerMainNode);
    }

    void DebugDrawerComponent::enableSelections(const std::string& layerName, const ::Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        selectableLayers.insert(layerName);
    }

    void DebugDrawerComponent::disableSelections(const std::string& layerName, const ::Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (layerName == "")
        {
            selectableLayers.clear();
        }
        else
        {
            selectableLayers.erase(layerName);
        }
    }

    void DebugDrawerComponent::clearSelections(const std::string& layerName, const Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        ScopedRecursiveLockPtr l2(new ScopedRecursiveLock(selectionMutex));

        if (layerName == "")
        {
            selectionNode->deselectAll();
        }
        else
        {
            selectionNode->deselect(layers.at(layerName).mainNode);
        }
    }

    void DebugDrawerComponent::selectionCallback()
    {
        ScopedRecursiveLockPtr l(new ScopedRecursiveLock(*topicMutex));
        listenerPrx->reportSelectionChanged(getSelections());
    }

    void DebugDrawerComponent::deselectionCallback()
    {
        ScopedRecursiveLockPtr l(new ScopedRecursiveLock(*topicMutex));
        listenerPrx->reportSelectionChanged(getSelections());
    }

    void DebugDrawerComponent::installSelectionCallbacks()
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        selectionNode->addSelectionCallback(selection_callback, this);
        selectionNode->addDeselectionCallback(deselection_callback, this);
    }

    void DebugDrawerComponent::removeSelectionCallbacks()
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        selectionNode->removeSelectionCallback(selection_callback, this);
        selectionNode->removeDeselectionCallback(deselection_callback, this);
    }

    void DebugDrawerComponent::reportSelectionChanged(const DebugDrawerSelectionList& selectedElements, const Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        ScopedRecursiveLockPtr l2(new ScopedRecursiveLock(selectionMutex));

        removeSelectionCallbacks();
        selectionNode->deselectAll();

        for (auto& e : selectedElements)
        {
            SoNode* n = SoSelection::getByName((SELECTION_NAME_PREFIX + e).c_str());
            if (n)
            {
                selectionNode->select(n);
            }
        }

        // Force visualization update
        selectionNode->touch();

        installSelectionCallbacks();
    }

    DebugDrawerSelectionList DebugDrawerComponent::getSelections(const ::Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        DebugDrawerSelectionList selectedElements;

        for (int i = 0; i < selectionNode->getNumSelected(); i++)
        {
            SoPath* path = selectionNode->getPath(i);

            std::string name = path->getTail()->getName().getString();
            if (name.length() > 0 && name.find(SELECTION_NAME_PREFIX) == 0)
            {
                name = name.substr(SELECTION_NAME_PREFIX.length());

                // Check if selected element is 'selectable'
                for (auto& layer : selectableLayers)
                {
                    if (layers[layer].addedBoxVisualizations.find(name) != layers[layer].addedBoxVisualizations.end()
                        || layers[layer].addedTextVisualizations.find(name) != layers[layer].addedTextVisualizations.end()
                        || layers[layer].addedSphereVisualizations.find(name) != layers[layer].addedSphereVisualizations.end()
                        || layers[layer].addedCylinderVisualizations.find(name) != layers[layer].addedCylinderVisualizations.end())
                    {
                        selectedElements.push_back(name);
                        break;
                    }
                }
            }
        }

        return selectedElements;
    }

    void DebugDrawerComponent::setVisuUpdateTime(float visuUpdatesPerSec)
    {
        if (timerSensor)
        {
            ARMARX_ERROR << "Cannot change the cycle time, once the thread has been started...";
            return;
        }

        if (visuUpdatesPerSec <= 0)
        {
            cycleTimeMS = 0;
        }
        else
        {
            cycleTimeMS = 1000.0f / (float)visuUpdatesPerSec;
        }
    }

    DebugDrawerComponent::~DebugDrawerComponent()
    {
        std::cout << "DESTRUCTOR debug drawer" << std::endl;
        /*{
            ScopedRecursiveLockPtr l = getScopedLock();
            layerMainNode->unref();
            coinVisu->unref();
        }*/
    }

    void DebugDrawerComponent::onInitComponent()
    {
        usingTopic(getProperty<std::string>("DebugDrawerTopic").getValue());
        bool enabled = getProperty<bool>("ShowDebugDrawing").getValue();

        if (!enabled)
        {
            disableAllLayers();
        }

        if (cycleTimeMS > 0)
        {
            SoSensorManager* sensor_mgr = SoDB::getSensorManager();
            timerSensor = new SoTimerSensor(updateVisualizationCB, this);
            timerSensor->setInterval(SbTime(cycleTimeMS / 1000.0f));
            sensor_mgr->insertTimerSensor(timerSensor);

            /*execTaskVisuUpdates = new PeriodicTask<DebugDrawerComponent>(this, &DebugDrawerComponent::updateVisualization, cycleTimeMS, false, "DebugDrawerComponentVisuUpdates");
            execTaskVisuUpdates->start();
            execTaskVisuUpdates->setDelayWarningTolerance(100);*/
        }

        offeringTopic(getProperty<std::string>("DebugDrawerSelectionTopic").getValue());
        usingTopic(getProperty<std::string>("DebugDrawerSelectionTopic").getValue());

        installSelectionCallbacks();
    }

    void DebugDrawerComponent::updateVisualizationCB(void* data, SoSensor* sensor)
    {
        DebugDrawerComponent* drawer = static_cast<DebugDrawerComponent*>(data);

        if (!drawer)
        {
            return;
        }

        drawer->updateVisualization();
    }


    void DebugDrawerComponent::onConnectComponent()
    {
        verbose = true;

        listenerPrx = getTopic<DebugDrawerListenerPrx>(getProperty<std::string>("DebugDrawerSelectionTopic").getValue());
    }

    void DebugDrawerComponent::onDisconnectComponent()
    {
        std::cout << "onDisconnectComponent debug drawer" << std::endl;
        /*verbose = false;
        size_t c = layers.size();
        size_t counter = 0;
        while (layers.size()>0 && counter < 2*c)
        {
            std::cout << "removing layer " << layers.begin()->first;
            removeLayer(layers.begin()->first);
            counter++; // sec counter
        }
        {
            ScopedRecursiveLockPtr l = getScopedLock();
            coinVisu->removeAllChildren();
        }*/
    }

    void DebugDrawerComponent::onExitComponent()
    {
        ARMARX_INFO << "onExitComponent debug drawer";
        verbose = false;

        //ARMARX_DEBUG << "onExitComponent";
        if (timerSensor)
        {
            SoSensorManager* sensor_mgr = SoDB::getSensorManager();
            sensor_mgr->removeTimerSensor(timerSensor);
        }

        size_t c = layers.size();
        size_t counter = 0;

        while (layers.size() > 0 && counter < 2 * c)
        {
            ARMARX_INFO << "removing layer " << layers.begin()->first;
            removeLayer(layers.begin()->first);
            counter++; // sec counter
        }

        {
            ScopedRecursiveLockPtr l = getScopedVisuLock();
            coinVisu->removeAllChildren();
            layerMainNode->unref();
            coinVisu->unref();
        }
    }

    void DebugDrawerComponent::exportScene(const std::string& filename, const Ice::Current&)
    {
        ARMARX_INFO << "Exporting scene to '" << filename << "'";

        SoWriteAction writeAction;
        writeAction.getOutput()->openFile(filename.c_str());
        writeAction.getOutput()->setBinary(false);
        writeAction.apply(layerMainNode);
        writeAction.getOutput()->closeFile();
    }

    void DebugDrawerComponent::exportLayer(const std::string& filename, const std::string& layerName, const Ice::Current&)
    {
        ARMARX_INFO << "Exporting layer '" << layerName << "' to '" << filename << "'";

        if (!hasLayer(layerName))
        {
            ARMARX_WARNING << "Unknown layer to export";
            return;
        }

        SoWriteAction writeAction;
        writeAction.getOutput()->openFile(filename.c_str());
        writeAction.getOutput()->setBinary(false);
        writeAction.apply(layers[layerName].mainNode);
        writeAction.getOutput()->closeFile();
    }

    void DebugDrawerComponent::drawCoordSystem(const CoordData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        ARMARX_DEBUG << "drawing coord system";

        auto& layer = requestLayer(d.layerName);

        removeCoordSystem(d.layerName, d.name);

        if (d.scale <= 0 || !d.active)
        {
            return;
        }

        //if (layer->addedCoordVisualizations.find(name) == layer->addedCoordVisualizations.end())
        //{
        SoSeparator* newS = new SoSeparator;
        SoMatrixTransform* newM = new SoMatrixTransform;
        newS->addChild(newM);
        std::string n = d.name;
        newS->addChild(CoinVisualizationFactory::CreateCoordSystemVisualization(d.scale, &n));
        layer.addedCoordVisualizations[d.name] = newS;
        layer.mainNode->addChild(newS);
        //}
        SoSeparator* s = layer.addedCoordVisualizations[d.name];
        SoMatrixTransform* m = (SoMatrixTransform*)(s->getChild(0));
        SbMatrix mNew = CoinVisualizationFactory::getSbMatrix(d.globalPose);
        m->matrix.setValue(mNew);

        ARMARX_DEBUG << "end";
    }

    void DebugDrawerComponent::drawLine(const LineData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        ARMARX_DEBUG << "drawLine1" << flush;

        auto& layer = requestLayer(d.layerName);

        removeLine(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        SoSeparator* newS = new SoSeparator;
        Eigen::Matrix4f lp1 = Eigen::Matrix4f::Identity();
        lp1(0, 3) = d.p1.x();
        lp1(1, 3) = d.p1.y();
        lp1(2, 3) = d.p1.z();
        Eigen::Matrix4f lp2 = Eigen::Matrix4f::Identity();
        lp2(0, 3) = d.p2.x();
        lp2(1, 3) = d.p2.y();
        lp2(2, 3) = d.p2.z();
        newS->addChild(CoinVisualizationFactory::createCoinLine(lp1, lp2, d.scale, d.color.r, d.color.g, d.color.b));
        layer.addedLineVisualizations[d.name] = newS;
        layer.mainNode->addChild(newS);
        ARMARX_DEBUG << "drawLine2" << flush;
    }

    void DebugDrawerComponent::drawLineSet(const DebugDrawerComponent::LineSetData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        ARMARX_DEBUG << "drawLineSet";

        auto& layer = requestLayer(d.layerName);

        removeLineSet(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        if (d.lineSet.points.size() % 2 != 0)
        {
            ARMARX_WARNING << "A line set requires an even amount of points";
            return;
        }

        if (d.lineSet.points.size() != 2 * d.lineSet.intensities.size())
        {
            ARMARX_WARNING << "Amounts of points and intensities have to match for a line set";
            return;
        }

        // Initialize color map for affordance visualization
        std::vector<VirtualRobot::VisualizationFactory::Color> colors;
        colors.push_back(VirtualRobot::VisualizationFactory::Color(d.lineSet.colorNoIntensity.r, d.lineSet.colorNoIntensity.g, d.lineSet.colorNoIntensity.b));
        colors.push_back(VirtualRobot::VisualizationFactory::Color(d.lineSet.colorFullIntensity.r, d.lineSet.colorFullIntensity.g, d.lineSet.colorFullIntensity.b));
        VirtualRobot::ColorMap visualizationColorMap = VirtualRobot::ColorMap::customColorMap(colors);

        // Initialize visualization nodes
        SoSeparator* sep = new SoSeparator;

        SoMaterialBinding* binding = new SoMaterialBinding;
        binding->value = SoMaterialBinding::PER_PART;
        sep->addChild(binding);

        SoDrawStyle* lineStyle = new SoDrawStyle;
        lineStyle->lineWidth.setValue(d.lineSet.lineWidth);
        sep->addChild(lineStyle);

        SoCoordinate3* coordinateNode = new SoCoordinate3;
        sep->addChild(coordinateNode);

        SoMaterial* materialNode = new SoMaterial;
        sep->addChild(materialNode);

        SoLineSet* lineSetNode = new SoLineSet;
        lineSetNode->startIndex.setValue(0);
        sep->addChild(lineSetNode);


        // Allocate index and material arrays
        SbVec3f* coordinateValues = new SbVec3f[d.lineSet.points.size()];
        int32_t* lineSetValues = new int32_t[d.lineSet.intensities.size()];
        SbColor* colorValues = new SbColor[d.lineSet.intensities.size()];

        for (unsigned int i = 0; i < d.lineSet.intensities.size(); i++)
        {
            lineSetValues[i] = 2;

            coordinateValues[2 * i].setValue(d.lineSet.points[2 * i].x, d.lineSet.points[2 * i].y, d.lineSet.points[2 * i].z);
            coordinateValues[2 * i + 1].setValue(d.lineSet.points[2 * i + 1].x, d.lineSet.points[2 * i + 1].y, d.lineSet.points[2 * i + 1].z);

            VirtualRobot::VisualizationFactory::Color c = visualizationColorMap.getColor(d.lineSet.intensities[i]);
            colorValues[i].setValue(c.r, c.g, c.b);
        }

        coordinateNode->point.setValuesPointer(d.lineSet.points.size(), coordinateValues);
        lineSetNode->numVertices.setValuesPointer(d.lineSet.intensities.size(), lineSetValues);
        materialNode->ambientColor.setValuesPointer(d.lineSet.intensities.size(), colorValues);
        materialNode->diffuseColor.setValuesPointer(d.lineSet.intensities.size(), colorValues);

        layer.addedLineSetVisualizations[d.name] = sep;
        layer.mainNode->addChild(sep);
    }

    void DebugDrawerComponent::drawBox(const BoxData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        ARMARX_DEBUG << "drawBox";

        auto& layer = requestLayer(d.layerName);

        removeBox(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        SoSeparator* newS = new SoSeparator;
        Eigen::Matrix4f m = d.globalPose;
        newS->addChild(CoinVisualizationFactory::getMatrixTransform(m));

        SoMaterial* material = new SoMaterial;
        material->ambientColor.setValue(d.color.r, d.color.g, d.color.b);
        material->diffuseColor.setValue(d.color.r, d.color.g, d.color.b);
        material->transparency.setValue(d.color.transparency);
        newS->addChild(material);

        SoCube* cube = new SoCube;
        cube->setName((SELECTION_NAME_PREFIX + d.name).c_str());
        cube->width = d.width;
        cube->height = d.height;
        cube->depth = d.depth;
        newS->addChild(cube);

        layer.addedBoxVisualizations[d.name] = newS;
        layer.mainNode->addChild(newS);
    }

    void DebugDrawerComponent::drawText(const TextData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        ARMARX_DEBUG << "drawText1";

        auto& layer = requestLayer(d.layerName);

        removeText(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        SoSeparator* sep = new SoSeparator;
        SoAnnotation* ann = new SoAnnotation;
        sep->addChild(ann);

        SoMaterial* mat = new SoMaterial;
        mat->ambientColor.setValue(d.color.r, d.color.g, d.color.b);
        mat->diffuseColor.setValue(d.color.r, d.color.g, d.color.b);
        mat->transparency.setValue(d.color.transparency);
        ann->addChild(mat);

        SoTransform* tr = new SoTransform;
        tr->translation.setValue(d.position.x(), d.position.y(), d.position.z());
        ann->addChild(tr);

        SoFont* font = new SoFont;
        font->name.setValue("TGS_Triplex_Roman");
        font->size = d.size;

        ann->addChild(font);

        SoText2* te = new SoText2;
        te->setName((SELECTION_NAME_PREFIX + d.name).c_str());
        te->string = d.text.c_str();
        te->justification = SoText2::CENTER;
        ann->addChild(te);

        layer.addedTextVisualizations[d.name] = sep;
        layer.mainNode->addChild(sep);
        ARMARX_DEBUG << "drawText2";
    }


    void DebugDrawerComponent::drawSphere(const SphereData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        ARMARX_DEBUG << "drawSphere";

        auto& layer = requestLayer(d.layerName);

        removeSphere(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        SoSeparator* sep = new SoSeparator;
        SoMaterial* mat = new SoMaterial;
        mat->ambientColor.setValue(d.color.r, d.color.g, d.color.b);
        mat->diffuseColor.setValue(d.color.r, d.color.g, d.color.b);
        mat->transparency.setValue(d.color.transparency);
        sep->addChild(mat);

        SoTransform* tr = new SoTransform;
        tr->translation.setValue(d.position.x(), d.position.y(), d.position.z());
        sep->addChild(tr);

        SoSphere* sphere = new SoSphere;
        sphere->setName((SELECTION_NAME_PREFIX + d.name).c_str());
        sphere->radius = d.radius;
        sep->addChild(sphere);

        layer.addedSphereVisualizations[d.name] = sep;
        layer.mainNode->addChild(sep);
    }

    void DebugDrawerComponent::drawCylinder(const DebugDrawerComponent::CylinderData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        ARMARX_DEBUG << "drawCylinder";

        auto& layer = requestLayer(d.layerName);

        removeCylinder(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        SoSeparator* sep = new SoSeparator;
        SoMaterial* mat = new SoMaterial;
        mat->ambientColor.setValue(d.color.r, d.color.g, d.color.b);
        mat->diffuseColor.setValue(d.color.r, d.color.g, d.color.b);
        mat->transparency.setValue(d.color.transparency);
        sep->addChild(mat);

        // The cylinder extends in y-direction, hence choose the transformation
        SoTransform* tr = new SoTransform;
        tr->translation.setValue(d.position.x(), d.position.y(), d.position.z());
        tr->rotation.setValue(SbRotation(SbVec3f(0, 1, 0), SbVec3f(d.direction.x(), d.direction.y(), d.direction.z())));
        sep->addChild(tr);

        SoCylinder* cylinder = new SoCylinder;
        cylinder->setName((SELECTION_NAME_PREFIX + d.name).c_str());
        cylinder->height = d.length;
        cylinder->radius = d.radius;
        sep->addChild(cylinder);

        layer.addedCylinderVisualizations[d.name] = sep;
        layer.mainNode->addChild(sep);
    }

    void DebugDrawerComponent::drawPointCloud(const PointCloudData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        auto& layer = requestLayer(d.layerName);

        removePointCloud(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        const auto& pcl = d.pointCloud.points;

        SoSeparator* pclSep = new SoSeparator;

        SoMaterial* pclMat = new SoMaterial;
        pclMat->diffuseColor.setValue(0.2, 0.2, 0.2);
        pclMat->diffuseColor.setValue(0.2, 0.2, 0.2);
        pclSep->addChild(pclMat);

        SoMaterialBinding* pclMatBind = new SoMaterialBinding;
        pclMatBind->value = SoMaterialBinding::OVERALL;
        pclSep->addChild(pclMatBind);

        SoCoordinate3* pclCoords = new SoCoordinate3;
        std::vector<SbVec3f> coords;
        coords.reserve(pcl.size());
        std::transform(
            pcl.begin(), pcl.end(), std::back_inserter(coords),
            [](const DebugDrawerPointCloudElement & elem)
        {
            return SbVec3f {elem.x, elem.y, elem.z};
        }
        );
        pclCoords->point.setValues(0, coords.size(), coords.data());
        pclSep->addChild(pclCoords);

        SoDrawStyle* pclStye = new SoDrawStyle;
        pclStye->pointSize = d.pointCloud.pointSize;
        pclSep->addChild(pclStye);

        pclSep->addChild(new SoPointSet);

        layer.addedPointCloudVisualizations[d.name] = pclSep;
        layer.mainNode->addChild(pclSep);
    }

    void DebugDrawerComponent::drawPolygon(const DebugDrawerComponent::PolygonData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        auto& layer = requestLayer(d.layerName);

        removePolygon(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        SoSeparator* sep = VirtualRobot::CoinVisualizationFactory::CreatePolygonVisualization(d.points, d.colorInner, d.colorBorder, d.lineWidth);
        layer.addedPolygonVisualizations[d.name] = sep;
        layer.mainNode->addChild(sep);
    }

    void DebugDrawerComponent::drawArrow(const DebugDrawerComponent::ArrowData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        auto& layer = requestLayer(d.layerName);

        removeArrow(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        SoSeparator* sep = new SoSeparator;

        SoTransform* tr = new SoTransform;
        tr->translation.setValue(d.position.x(), d.position.y(), d.position.z());
        sep->addChild(tr);

        SoSeparator* sepArrow = VirtualRobot::CoinVisualizationFactory::CreateArrow(d.direction, d.length, d.width, d.color);
        sep->addChild(sepArrow);

        layer.addedArrowVisualizations[d.name] = sep;
        layer.mainNode->addChild(sep);
    }

    void DebugDrawerComponent::ensureExistingRobotNodes(DebugDrawerComponent::RobotData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!d.active)
        {
            return;
        }

        // load or get robot
        RobotPtr rob = requestRobot(d);

        if (!rob)
        {
            ARMARX_ERROR << deactivateSpam() << "Could not determine robot " << d.name << " at layer " << d.layerName;
            return;
        }

        std::map < std::string, float >::iterator i = d.configuration.begin();
        while (i != d.configuration.end())
        {
            if (!rob->hasRobotNode(i->first))
            {
                ARMARX_WARNING << deactivateSpam() << "Robot " << rob->getName() << " does not know RobotNode " << i->first;
                i = d.configuration.erase(i);
            }
            else
            {
                i++;
            }
        }
    }

    void DebugDrawerComponent::drawRobot(const DebugDrawerComponent::RobotData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        auto& layer = requestLayer(d.layerName);

        if (!d.active)
        {
            removeRobot(d.layerName, d.name);
            return;
        }

        // load or get robot
        RobotPtr rob = requestRobot(d);

        if (!rob)
        {
            ARMARX_ERROR << deactivateSpam() << "Could not determine robot " << d.name << " at layer " << d.layerName;
            return;
        }

        if (d.updatePose)
        {
            rob->setGlobalPose(d.globalPose);
        }

        if (d.updateConfig)
        {
            rob->setJointValues(d.configuration);
        }

        if (d.updateColor)
        {
            if (layer.addedRobotVisualizations.find(d.name) == layer.addedRobotVisualizations.end())
            {
                ARMARX_WARNING << deactivateSpam() << "Internal robot visu error";
            }
            else
            {
                SoSeparator* sep = layer.addedRobotVisualizations[d.name];

                if (!sep || sep->getNumChildren() < 2)
                {
                    ARMARX_ERROR << "Internal robot layer error1";
                    return;
                }

                SoMaterial* m = dynamic_cast<SoMaterial*>(sep->getChild(0));

                if (!m)
                {
                    ARMARX_ERROR << "Internal robot layer error2";
                    return;
                }

                if (d.color.isNone())
                {
                    m->setOverride(false);
                }
                else
                {
                    if (d.color.r < 0 || d.color.g < 0 || d.color.b < 0)
                    {

                    }

                    m->diffuseColor = SbColor(d.color.r, d.color.g, d.color.b);
                    m->ambientColor = SbColor(0, 0, 0);
                    m->transparency = std::max(0.0f, d.color.transparency);
                    m->setOverride(true);
                }
            }
        }
    }

    VirtualRobot::RobotPtr DebugDrawerComponent::requestRobot(const DebugDrawerComponent::RobotData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        auto& layer = requestLayer(d.layerName);
        std::string entryName = "__" + d.layerName + "__" + d.name + "__";

        ARMARX_DEBUG << "Requesting robot " << entryName;

        if (activeRobots.find(entryName) != activeRobots.end())
        {
            ARMARX_DEBUG << "Found robot " << entryName << ":" << activeRobots[entryName]->getName();
            return activeRobots[entryName];
        }

        VirtualRobot::RobotPtr result;

        if (d.robotFile.empty())
        {
            ARMARX_INFO << deactivateSpam() << "No robot defined for layer " << d.layerName << ", with name " << d.name;
            return result;
        }

        if (!d.armarxProject.empty())
        {
            ARMARX_INFO << "Adding to datapaths of " << d.armarxProject;
            armarx::CMakePackageFinder finder(d.armarxProject);

            if (!finder.packageFound())
            {
                ARMARX_WARNING << "ArmarX Package " << d.armarxProject << " has not been found!";
            }
            else
            {
                ARMARX_INFO << "Adding to datapaths: " << finder.getDataDir();
                armarx::ArmarXDataPath::addDataPaths(finder.getDataDir());
            }
        }

        // load robot
        std::string filename = d.robotFile;
        ArmarXDataPath::getAbsolutePath(filename, filename);
        ARMARX_INFO << "Loading robot from " << filename;

        try
        {
            result = RobotIO::loadRobot(filename);
        }
        catch (...)
        {

        }

        if (!result)
        {
            ARMARX_IMPORTANT << "Robot loading failed, file:" << filename;
            return result;
        }

        SoSeparator* sep = new SoSeparator;
        layer.mainNode->addChild(sep);

        SoMaterial* m = new SoMaterial;
        m->setOverride(false);
        sep->addChild(m);



        VirtualRobot::SceneObject::VisualizationType visuType = VirtualRobot::SceneObject::Full;

        if (d.drawStyle == DrawStyle::CollisionModel)
        {
            visuType = VirtualRobot::SceneObject::Collision;
        }

        boost::shared_ptr<CoinVisualization> robVisu = result->getVisualization<CoinVisualization>(visuType);

        if (robVisu)
        {
            SoNode* sepRob = robVisu->getCoinVisualization();

            if (sepRob)
            {
                sep->addChild(sepRob);
            }
        }

        activeRobots[entryName] = result;
        ARMARX_DEBUG << "setting robot to activeRobots, entryName:" << entryName << ", activeRobots.size():" << activeRobots.size();
        layer.addedRobotVisualizations[d.name] = sep;
        ARMARX_DEBUG << "adding sep to layer.addedRobotVisualizations, d.name:" << d.name << ", layer:" << d.layerName << ", layer.addedRobotVisualizations.size():" << layer.addedRobotVisualizations.size();

        return result;
    }

    void DebugDrawerComponent::removeLine(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedLineVisualizations.find(name) == layer.addedLineVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedLineVisualizations[name]);
        layer.addedLineVisualizations.erase(name);
    }

    void DebugDrawerComponent::removeLineSet(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedLineSetVisualizations.find(name) == layer.addedLineSetVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedLineSetVisualizations[name]);
        layer.addedLineSetVisualizations.erase(name);
    }

    void DebugDrawerComponent::removeBox(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedBoxVisualizations.find(name) == layer.addedBoxVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedBoxVisualizations[name]);
        layer.addedBoxVisualizations.erase(name);
    }

    void DebugDrawerComponent::removeText(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedTextVisualizations.find(name) == layer.addedTextVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedTextVisualizations[name]);
        layer.addedTextVisualizations.erase(name);
    }

    void DebugDrawerComponent::removeSphere(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedSphereVisualizations.find(name) == layer.addedSphereVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedSphereVisualizations[name]);
        layer.addedSphereVisualizations.erase(name);
    }

    void DebugDrawerComponent::removeCylinder(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedCylinderVisualizations.find(name) == layer.addedCylinderVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedCylinderVisualizations[name]);
        layer.addedCylinderVisualizations.erase(name);
    }

    void DebugDrawerComponent::removePointCloud(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedPointCloudVisualizations.find(name) == layer.addedPointCloudVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedPointCloudVisualizations[name]);
        layer.addedPointCloudVisualizations.erase(name);
    }

    void DebugDrawerComponent::removePolygon(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedPolygonVisualizations.find(name) == layer.addedPolygonVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedPolygonVisualizations[name]);
        layer.addedPolygonVisualizations.erase(name);
    }

    void DebugDrawerComponent::removeArrow(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedArrowVisualizations.find(name) == layer.addedArrowVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedArrowVisualizations[name]);
        layer.addedArrowVisualizations.erase(name);
    }

    void DebugDrawerComponent::removeRobot(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        // process active robots
        std::string entryName = "__" + layerName + "__" + name + "__";
        ARMARX_DEBUG << "Removing robot " << entryName;

        if (activeRobots.find(entryName) != activeRobots.end())
        {
            ARMARX_DEBUG << "Found robot to remove " << entryName;
            activeRobots.erase(entryName);
            ARMARX_DEBUG << "after Found robot to remove, activeRobots.size() = " << activeRobots.size();
        }

        // process visualizations
        if (!hasLayer(layerName))
        {
            ARMARX_DEBUG << "Layer not found " << layerName;
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedRobotVisualizations.find(name) == layer.addedRobotVisualizations.end())
        {
            ARMARX_INFO << "Could not find robot with name " << name;
            return;
        }

        ARMARX_DEBUG << "Removing visualization for " << entryName;

        ARMARX_DEBUG << "removing sep from layer.addedRobotVisualizations, d.name:" << name << ", layer:" << layerName << ", layer.addedRobotVisualizations.size():" << layer.addedRobotVisualizations.size();

        if (layer.addedRobotVisualizations.find(name) == layer.addedRobotVisualizations.end())
        {
            ARMARX_WARNING << "separator not found...";
            return;
        }

        ARMARX_DEBUG << "removing from layer.mainNode, layer.mainNode->getNumChildren()=" << layer.mainNode->getNumChildren();

        if (layer.mainNode->findChild(layer.addedRobotVisualizations[name]) < 0)
        {
            ARMARX_WARNING << "separator with wrong index...";
            return;
        }

        layer.mainNode->removeChild(layer.addedRobotVisualizations[name]);
        layer.addedRobotVisualizations.erase(name);
        ARMARX_DEBUG << "after removing from layer.mainNode, layer.mainNode->getNumChildren()=" << layer.mainNode->getNumChildren();
        ARMARX_DEBUG << "after removing sep from layer.addedRobotVisualizations, d.name:" << name << ", layer:" << layerName << ", layer.addedRobotVisualizations.size():" << layer.addedRobotVisualizations.size();
    }

    void DebugDrawerComponent::removeCoordSystem(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedCoordVisualizations.find(name) == layer.addedCoordVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedCoordVisualizations[name]);
        layer.addedCoordVisualizations.erase(name);
    }

    void DebugDrawerComponent::setLayerVisibility(const std::string& layerName, bool visible)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);
        layer.visible = visible;

        if (visible)
        {
            if (layerMainNode->findChild(layer.mainNode) < 0)
            {
                layerMainNode->addChild(layer.mainNode);
            }
        }
        else
        {
            if (layerMainNode->findChild(layer.mainNode) >= 0)
            {
                layerMainNode->removeChild(layer.mainNode);
            }
        }
    }

    void DebugDrawerComponent::disableAllLayers(const ::Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (selectionNode->findChild(layerMainNode) >= 0)
        {
            selectionNode->removeChild(layerMainNode);
        }
    }

    void DebugDrawerComponent::enableAllLayers(const ::Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (selectionNode->findChild(layerMainNode) < 0)
        {
            selectionNode->addChild(layerMainNode);
        }
    }

    void DebugDrawerComponent::setScaledPoseVisu(const std::string& layerName, const std::string& poseName, const ::armarx::PoseBasePtr& globalPose, const ::Ice::Float scale, const ::Ice::Current&)
    {
        ARMARX_DEBUG << VAROUT(layerName) << VAROUT(poseName);
        Eigen::Matrix4f gp = PosePtr::dynamicCast(globalPose)->toEigen();
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + poseName + "__";
            CoordData& d = accumulatedUpdateData.coord[entryName];
            d.globalPose = gp;
            d.layerName = layerName;
            d.name = poseName;
            d.scale = scale;
            d.active = true;
        }
    }

    void DebugDrawerComponent::setScaledPoseDebugLayerVisu(const std::string& poseName, const ::armarx::PoseBasePtr& globalPose, const ::Ice::Float scale, const ::Ice::Current&)
    {
        setScaledPoseVisu(DEBUG_LAYER_NAME, poseName, globalPose, scale);
    }

    void DebugDrawerComponent::setPoseVisu(const std::string& layerName, const std::string& poseName, const PoseBasePtr& globalPose, const Ice::Current&)
    {
        setScaledPoseVisu(layerName, poseName, globalPose, 1.f);
    }

    void DebugDrawerComponent::setPoseDebugLayerVisu(const std::string& poseName, const PoseBasePtr& globalPose, const Ice::Current&)
    {
        setScaledPoseVisu(DEBUG_LAYER_NAME, poseName, globalPose, 1.f);
    }

    void DebugDrawerComponent::removePoseVisu(const std::string& layerName, const std::string& poseName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + poseName + "__";
            CoordData& d = accumulatedUpdateData.coord[entryName];
            d.layerName = layerName;
            d.name = poseName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removePoseDebugLayerVisu(const std::string& poseName, const Ice::Current&)
    {
        removePoseVisu(DEBUG_LAYER_NAME, poseName);
    }

    void DebugDrawerComponent::setLineVisu(const std::string& layerName, const std::string& lineName, const Vector3BasePtr& globalPosition1, const Vector3BasePtr& globalPosition2, float lineWidth, const DrawColor& color, const Ice::Current&)
    {
        Eigen::Vector3f p1 = Vector3Ptr::dynamicCast(globalPosition1)->toEigen();
        Eigen::Vector3f p2 = Vector3Ptr::dynamicCast(globalPosition2)->toEigen();
        VirtualRobot::VisualizationFactory::Color c(color.r, color.g, color.b, 1 - color.a);
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + lineName + "__";
            LineData& d = accumulatedUpdateData.line[entryName];
            d.p1 = p1;
            d.p2 = p2;
            d.layerName = layerName;
            d.name = lineName;
            d.color = c;
            d.scale = lineWidth;
            d.active = true;
        }
    }

    void DebugDrawerComponent::setLineDebugLayerVisu(const std::string& lineName, const Vector3BasePtr& globalPosition1, const Vector3BasePtr& globalPosition2, float lineWidth, const DrawColor& color, const Ice::Current&)
    {
        setLineVisu(DEBUG_LAYER_NAME, lineName, globalPosition1, globalPosition2, lineWidth, color);
    }

    void DebugDrawerComponent::removeLineVisu(const std::string& layerName, const std::string& lineName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + lineName + "__";
            LineData& d = accumulatedUpdateData.line[entryName];
            d.layerName = layerName;
            d.name = lineName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removeLineDebugLayerVisu(const std::string& lineName, const Ice::Current&)
    {
        removeLineVisu(DEBUG_LAYER_NAME, lineName);
    }

    void DebugDrawerComponent::setLineSetVisu(const std::string& layerName, const std::string& lineSetName, const DebugDrawerLineSet& lineSet, const Ice::Current&)
    {
        ARMARX_DEBUG << VAROUT(layerName) << VAROUT(lineSetName);
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + lineSetName + "__";
            LineSetData& d = accumulatedUpdateData.lineSet[entryName];
            d.lineSet = lineSet;
            d.layerName = layerName;
            d.name = lineSetName;
            d.active = true;
        }
    }

    void DebugDrawerComponent::setLineSetDebugLayerVisu(const std::string& lineSetName, const DebugDrawerLineSet& lineSet, const Ice::Current&)
    {
        setLineSetVisu(DEBUG_LAYER_NAME, lineSetName, lineSet);
    }

    void DebugDrawerComponent::removeLineSetVisu(const std::string& layerName, const std::string& lineSetName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + lineSetName + "__";
            LineSetData& d = accumulatedUpdateData.lineSet[entryName];
            d.layerName = layerName;
            d.name = lineSetName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removeLineSetDebugLayerVisu(const std::string& lineSetName, const Ice::Current&)
    {
        removeLineSetVisu(DEBUG_LAYER_NAME, lineSetName);
    }

    void DebugDrawerComponent::setBoxVisu(const std::string& layerName, const std::string& boxName, const PoseBasePtr& globalPose, const Vector3BasePtr& dimensions, const DrawColor& color, const Ice::Current&)
    {
        Eigen::Matrix4f gp = PosePtr::dynamicCast(globalPose)->toEigen();
        VirtualRobot::VisualizationFactory::Color c(color.r, color.g, color.b, 1 - color.a);
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + boxName + "__";
            BoxData& d = accumulatedUpdateData.box[entryName];
            d.width = dimensions->x;
            d.height = dimensions->y;
            d.depth = dimensions->z;
            d.layerName = layerName;
            d.name = boxName;
            d.color = c;
            d.globalPose = gp;
            d.active = true;
        }
    }

    void DebugDrawerComponent::setBoxDebugLayerVisu(const std::string& boxName, const PoseBasePtr& globalPose, const Vector3BasePtr& dimensions, const DrawColor& color, const Ice::Current&)
    {
        setBoxVisu(DEBUG_LAYER_NAME, boxName, globalPose, dimensions, color);
    }

    void DebugDrawerComponent::removeBoxVisu(const std::string& layerName, const std::string& boxName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + boxName + "__";
            BoxData& d = accumulatedUpdateData.box[entryName];
            d.layerName = layerName;
            d.name = boxName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removeBoxDebugLayerVisu(const std::string& boxName, const Ice::Current&)
    {
        removeBoxVisu(DEBUG_LAYER_NAME, boxName);
    }

    void DebugDrawerComponent::setTextVisu(const std::string& layerName, const std::string& textName, const std::string& text, const Vector3BasePtr& globalPosition, const DrawColor& color, int size, const Ice::Current&)
    {
        ARMARX_DEBUG << VAROUT(layerName) << VAROUT(textName);
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + textName + "__";
            TextData& d = accumulatedUpdateData.text[entryName];
            d.text = text;
            d.position = Vector3Ptr::dynamicCast(globalPosition)->toEigen();
            d.layerName = layerName;
            d.name = textName;
            d.color = VirtualRobot::VisualizationFactory::Color(color.r, color.g, color.b, 1 - color.a);
            d.size = size;
            d.active = true;
        }
    }

    void DebugDrawerComponent::setTextDebugLayerVisu(const std::string& textName, const std::string& text, const Vector3BasePtr& globalPosition, const DrawColor& color, int size, const Ice::Current&)
    {
        setTextVisu(DEBUG_LAYER_NAME, textName, text, globalPosition, color, size);
    }

    void DebugDrawerComponent::removeTextVisu(const std::string& layerName, const std::string& textName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + textName + "__";
            TextData& d = accumulatedUpdateData.text[entryName];
            d.layerName = layerName;
            d.name = textName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removeTextDebugLayerVisu(const std::string& textName, const Ice::Current&)
    {
        removeTextVisu(DEBUG_LAYER_NAME, textName);
    }

    void DebugDrawerComponent::setSphereVisu(const std::string& layerName, const std::string& sphereName, const Vector3BasePtr& globalPosition, const DrawColor& color, float radius, const Ice::Current&)
    {
        ARMARX_DEBUG << VAROUT(layerName) << VAROUT(sphereName);
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + sphereName + "__";
            SphereData& d = accumulatedUpdateData.sphere[entryName];
            d.radius = radius;
            d.position = Vector3Ptr::dynamicCast(globalPosition)->toEigen();
            d.layerName = layerName;
            d.name = sphereName;
            d.color = VirtualRobot::VisualizationFactory::Color(color.r, color.g, color.b, 1 - color.a);
            d.active = true;
        }
    }

    void DebugDrawerComponent::setSphereDebugLayerVisu(const std::string& sphereName, const Vector3BasePtr& globalPosition, const DrawColor& color, float radius, const Ice::Current&)
    {
        setSphereVisu(DEBUG_LAYER_NAME, sphereName, globalPosition, color, radius);
    }

    void DebugDrawerComponent::removeSphereVisu(const std::string& layerName, const std::string& sphereName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + sphereName + "__";
            SphereData& d = accumulatedUpdateData.sphere[entryName];
            d.layerName = layerName;
            d.name = sphereName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removeSphereDebugLayerVisu(const std::string& sphereName, const Ice::Current&)
    {
        removeSphereVisu(DEBUG_LAYER_NAME, sphereName);
    }

    void DebugDrawerComponent::setCylinderVisu(const std::string& layerName, const std::string& cylinderName, const Vector3BasePtr& globalPosition, const Vector3BasePtr& direction, float length, float radius, const DrawColor& color, const Ice::Current&)
    {
        ARMARX_DEBUG << VAROUT(layerName) << VAROUT(cylinderName);
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + cylinderName + "__";
            CylinderData& d = accumulatedUpdateData.cylinder[entryName];
            d.position = Vector3Ptr::dynamicCast(globalPosition)->toEigen();
            d.direction = Vector3Ptr::dynamicCast(direction)->toEigen();
            d.length = length;
            d.radius = radius;
            d.layerName = layerName;
            d.name = cylinderName;
            d.color = VirtualRobot::VisualizationFactory::Color(color.r, color.g, color.b, 1 - color.a);
            d.active = true;
        }
    }

    void DebugDrawerComponent::setCylinderDebugLayerVisu(const std::string& cylinderName, const Vector3BasePtr& globalPosition, const Vector3BasePtr& direction, float length, float radius, const DrawColor& color, const Ice::Current&)
    {
        setCylinderVisu(DEBUG_LAYER_NAME, cylinderName, globalPosition, direction, length, radius, color);
    }

    void DebugDrawerComponent::removeCylinderVisu(const std::string& layerName, const std::string& cylinderName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + cylinderName + "__";
            CylinderData& d = accumulatedUpdateData.cylinder[entryName];
            d.layerName = layerName;
            d.name = cylinderName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removeCylinderDebugLayerVisu(const std::string& cylinderName, const Ice::Current&)
    {
        removeCylinderVisu(DEBUG_LAYER_NAME, cylinderName);
    }

    void DebugDrawerComponent::setPointCloudVisu(const std::string& layerName, const std::string& pointCloudName, const DebugDrawerPointCloud& pointCloud, const Ice::Current&)
    {
        ARMARX_DEBUG << VAROUT(layerName) << VAROUT(pointCloudName);
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + pointCloudName + "__";
            PointCloudData& d = accumulatedUpdateData.pointcloud[entryName];
            d.pointCloud = pointCloud;
            d.layerName = layerName;
            d.name = pointCloudName;
            d.active = true;
        }
    }

    void DebugDrawerComponent::setPointCloudDebugLayerVisu(const std::string& pointCloudName, const DebugDrawerPointCloud& pointCloud, const Ice::Current&)
    {
        setPointCloudVisu(DEBUG_LAYER_NAME, pointCloudName, pointCloud);
    }

    void DebugDrawerComponent::removePointCloudVisu(const std::string& layerName, const std::string& pointCloudName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + pointCloudName + "__";
            PointCloudData& d = accumulatedUpdateData.pointcloud[entryName];
            d.layerName = layerName;
            d.name = pointCloudName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removePointCloudDebugLayerVisu(const std::string& pointCloudName, const Ice::Current&)
    {
        removePointCloudVisu(DEBUG_LAYER_NAME, pointCloudName);
    }

    void DebugDrawerComponent::setPolygonVisu(const std::string& layerName, const std::string& polygonName, const std::vector<Vector3BasePtr>& polygonPoints, const DrawColor& colorInner, const DrawColor& colorBorder, float lineWidth, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + polygonName + "__";
            PolygonData& d = accumulatedUpdateData.polygons[entryName];

            std::vector< Eigen::Vector3f > points;

            for (size_t i = 0; i < polygonPoints.size(); i++)
            {
                Eigen::Vector3f p = Vector3Ptr::dynamicCast(polygonPoints.at(i))->toEigen();;
                points.push_back(p);
            }

            d.points = points;
            d.colorInner = VirtualRobot::VisualizationFactory::Color(colorInner.r, colorInner.g, colorInner.b, 1 - colorInner.a);;
            d.colorBorder = VirtualRobot::VisualizationFactory::Color(colorBorder.r, colorBorder.g, colorBorder.b, 1 - colorBorder.a);;

            d.lineWidth = lineWidth;
            d.layerName = layerName;
            d.name = polygonName;
            d.active = true;
        }
    }

    void DebugDrawerComponent::setPolygonDebugLayerVisu(const std::string& polygonName, const std::vector<Vector3BasePtr>& polygonPoints, const DrawColor& colorInner, const DrawColor& colorBorder, float lineWidth, const Ice::Current&)
    {
        setPolygonVisu(DEBUG_LAYER_NAME, polygonName, polygonPoints, colorInner, colorBorder, lineWidth);
    }

    void DebugDrawerComponent::removePolygonVisu(const std::string& layerName, const std::string& polygonName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + polygonName + "__";
            PolygonData& d = accumulatedUpdateData.polygons[entryName];
            d.layerName = layerName;
            d.name = polygonName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removePolygonDebugLayerVisu(const std::string& polygonName, const Ice::Current&)
    {
        removePolygonVisu(DEBUG_LAYER_NAME, polygonName);
    }

    void DebugDrawerComponent::setArrowVisu(const std::string& layerName, const std::string& arrowName, const Vector3BasePtr& position, const Vector3BasePtr& direction, const DrawColor& color, float length, float width, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + arrowName + "__";
            ArrowData& d = accumulatedUpdateData.arrows[entryName];

            d.position = Vector3Ptr::dynamicCast(position)->toEigen();
            d.direction = Vector3Ptr::dynamicCast(direction)->toEigen();
            d.color = VirtualRobot::VisualizationFactory::Color(color.r, color.g, color.b, 1 - color.a);;
            d.length = length;
            d.width = width;

            d.layerName = layerName;
            d.name = arrowName;
            d.active = true;
        }
    }

    void DebugDrawerComponent::setArrowDebugLayerVisu(const std::string& arrowName, const Vector3BasePtr& position, const Vector3BasePtr& direction, const DrawColor& color, float length, float width, const Ice::Current&)
    {
        setArrowVisu(DEBUG_LAYER_NAME, arrowName, position, direction, color, length, width);
    }

    void DebugDrawerComponent::removeArrowVisu(const std::string& layerName, const std::string& arrowName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + arrowName + "__";
            ArrowData& d = accumulatedUpdateData.arrows[entryName];
            d.layerName = layerName;
            d.name = arrowName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removeArrowDebugLayerVisu(const std::string& arrowName, const Ice::Current&)
    {
        removeArrowVisu(DEBUG_LAYER_NAME, arrowName);
    }

    void DebugDrawerComponent::setRobotVisu(const std::string& layerName, const std::string& robotName, const std::string& robotFile, const std::string& armarxProject, DrawStyle drawStyle, const Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
        std::string entryName = "__" + layerName + "__" + robotName + "__";
        ARMARX_DEBUG << "setting robot visualization for " << entryName;
        RobotData& d = accumulatedUpdateData.robots[entryName];

        d.robotFile = robotFile;
        d.armarxProject = armarxProject;
        d.drawStyle = drawStyle;

        d.layerName = layerName;
        d.name = robotName;
        d.active = true;
    }

    void DebugDrawerComponent::updateRobotPose(const std::string& layerName, const std::string& robotName, const PoseBasePtr& globalPose, const Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
        std::string entryName = "__" + layerName + "__" + robotName + "__";
        ARMARX_DEBUG << "updating robot pose for " << entryName;
        RobotData& d = accumulatedUpdateData.robots[entryName];

        // update data
        d.update = true;
        d.updatePose = true;
        d.globalPose = PosePtr::dynamicCast(globalPose)->toEigen();

        d.layerName = layerName;
        d.name = robotName;
        d.active = true;
    }

    void DebugDrawerComponent::updateRobotConfig(const std::string& layerName, const std::string& robotName, const std::map<std::string, float>& configuration, const Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
        std::string entryName = "__" + layerName + "__" + robotName + "__";
        ARMARX_DEBUG << "updating robot config for " << entryName;
        RobotData& d = accumulatedUpdateData.robots[entryName];

        // update data
        d.update = true;
        d.updateConfig = true;
        d.layerName = layerName;
        d.name = robotName;

        for (auto& it : configuration)
        {
            d.configuration[it.first] = it.second;
        }

        d.active = true;
    }

    void DebugDrawerComponent::updateRobotColor(const std::string& layerName, const std::string& robotName, const DrawColor& c, const Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
        std::string entryName = "__" + layerName + "__" + robotName + "__";
        ARMARX_DEBUG << "updating robot color for " << entryName;
        RobotData& d = accumulatedUpdateData.robots[entryName];

        // update data
        d.update = true;
        d.updateColor = true;
        d.layerName = layerName;
        d.name = robotName;

        if (c.a == 0 && c.b == 0 && c.r == 0 && c.g == 0)
        {
            d.color = VirtualRobot::VisualizationFactory::Color::None();
        }
        else
        {
            d.color = VirtualRobot::VisualizationFactory::Color(c.r, c.g, c.b, 1 - c.a);
        }

        d.active = true;
    }

    void DebugDrawerComponent::removeRobotVisu(const std::string& layerName, const std::string& robotName, const Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
        std::string entryName = "__" + layerName + "__" + robotName + "__";
        ARMARX_DEBUG << "removing robot visu for " << entryName;
        RobotData& d = accumulatedUpdateData.robots[entryName];

        d.layerName = layerName;
        d.name = robotName;
        d.active = false;
    }

    void DebugDrawerComponent::clearAll(const Ice::Current&)
    {
        for (auto& i : layers)
        {
            clearLayer(i.first);
        }
    }

    void DebugDrawerComponent::clearLayer(const std::string& layerName, const Ice::Current&)
    {
        ScopedRecursiveLockPtr lockData = getScopedAccumulatedDataLock();

        // check if already requested
        if (std::find(accumulatedUpdateData.clearLayers.begin(), accumulatedUpdateData.clearLayers.end(), layerName) != accumulatedUpdateData.clearLayers.end())
        {
            return;
        }

        accumulatedUpdateData.clearLayers.push_back(layerName);
    }

    void DebugDrawerComponent::clearLayerQt(const std::string& layerName)
    {
        ScopedRecursiveLockPtr lockData = getScopedAccumulatedDataLock();
        ScopedRecursiveLockPtr lockVisu = getScopedVisuLock();

        ARMARX_DEBUG << "clearing layer " << layerName;

        if (!hasLayer(layerName))
        {
            if (verbose)
            {
                ARMARX_VERBOSE << "Layer " << layerName << " can't be cleared, because it does not exist.";
            }

            return;
        }

        if (verbose)
        {
            ARMARX_VERBOSE << "Clearing layer " << layerName;
        }

        selectionNode->deselectAll();

        removeAccumulatedData(layerName);

        Layer& layer = layers.at(layerName);

        for (const auto& i : layer.addedCoordVisualizations)
        {
            removePoseVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedLineVisualizations)
        {
            removeLineVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedLineSetVisualizations)
        {
            removeLineSetVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedBoxVisualizations)
        {
            removeBoxVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedTextVisualizations)
        {
            removeTextVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedSphereVisualizations)
        {
            removeSphereVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedCylinderVisualizations)
        {
            removeCylinderVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedPointCloudVisualizations)
        {
            removePointCloudVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedPolygonVisualizations)
        {
            removePolygonVisu(layerName, i.first);
        }

        for (const auto& i : layer.added24BitColoredPointCloudVisualizations)
        {
            remove24BitColoredPointCloudVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedArrowVisualizations)
        {
            removeArrowVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedRobotVisualizations)
        {
            removeRobotVisu(layerName, i.first);
        }

        for (const auto& i : layer.addedCustomVisualizations)
        {
            removeCustomVisu(layerName, i.first);
        }
    }

    void DebugDrawerComponent::clearDebugLayer(const Ice::Current&)
    {
        clearLayer(DEBUG_LAYER_NAME);
    }

    void DebugDrawerComponent::setMutex(boost::shared_ptr<boost::recursive_mutex> m)
    {
        //ARMARX_IMPORTANT << "set mutex3d:" << m.get();
        mutex = m;
    }

    ScopedRecursiveLockPtr DebugDrawerComponent::getScopedVisuLock()
    {
        ScopedRecursiveLockPtr l;

        if (mutex)
        {
            //ARMARX_IMPORTANT << mutex.get();
            l.reset(new ScopedRecursiveLock(*mutex));
        }
        else
        {
            ARMARX_IMPORTANT << deactivateSpam(5) << "NO 3D MUTEX!!!";
        }

        return l;
    }

    ScopedRecursiveLockPtr DebugDrawerComponent::getScopedAccumulatedDataLock()
    {
        ScopedRecursiveLockPtr l(new ScopedRecursiveLock(*dataUpdateMutex));
        return l;
    }

    SoSeparator* DebugDrawerComponent::getVisualization()
    {
        return coinVisu;
    }

    DebugDrawerComponent::Layer& DebugDrawerComponent::requestLayer(const std::string& layerName)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (hasLayer(layerName))
        {
            return layers.at(layerName);
        }

        ARMARX_VERBOSE << "Created layer " << layerName;

        SoSeparator* mainNode = new SoSeparator {};
        mainNode->ref();
        layerMainNode->addChild(mainNode);
        layers[layerName] = Layer();
        layers.at(layerName).mainNode = mainNode;
        layers.at(layerName).visible = true;
        return layers.at(layerName);
    }

    void DebugDrawerComponent::updateVisualization()
    {
        ScopedRecursiveLockPtr lockData = getScopedAccumulatedDataLock();
        ScopedRecursiveLockPtr lockVisu = getScopedVisuLock();

        for (auto i = accumulatedUpdateData.coord.begin(); i != accumulatedUpdateData.coord.end(); i++)
        {
            drawCoordSystem(i->second);
        }

        accumulatedUpdateData.coord.clear();

        for (auto i = accumulatedUpdateData.box.begin(); i != accumulatedUpdateData.box.end(); i++)
        {
            drawBox(i->second);
        }

        accumulatedUpdateData.box.clear();

        for (auto i = accumulatedUpdateData.line.begin(); i != accumulatedUpdateData.line.end(); i++)
        {
            drawLine(i->second);
        }

        accumulatedUpdateData.line.clear();

        for (auto i = accumulatedUpdateData.lineSet.begin(); i != accumulatedUpdateData.lineSet.end(); i++)
        {
            drawLineSet(i->second);
        }

        accumulatedUpdateData.lineSet.clear();

        for (auto i = accumulatedUpdateData.sphere.begin(); i != accumulatedUpdateData.sphere.end(); i++)
        {
            drawSphere(i->second);
        }

        accumulatedUpdateData.sphere.clear();

        for (auto i = accumulatedUpdateData.cylinder.begin(); i != accumulatedUpdateData.cylinder.end(); i++)
        {
            drawCylinder(i->second);
        }

        accumulatedUpdateData.cylinder.clear();

        for (auto i = accumulatedUpdateData.text.begin(); i != accumulatedUpdateData.text.end(); i++)
        {
            drawText(i->second);
        }

        accumulatedUpdateData.text.clear();

        for (auto i = accumulatedUpdateData.pointcloud.begin(); i != accumulatedUpdateData.pointcloud.end(); i++)
        {
            drawPointCloud(i->second);
        }

        accumulatedUpdateData.pointcloud.clear();

        for (auto i = accumulatedUpdateData.polygons.begin(); i != accumulatedUpdateData.polygons.end(); i++)
        {
            drawPolygon(i->second);
        }

        accumulatedUpdateData.polygons.clear();

        for (auto i = accumulatedUpdateData.arrows.begin(); i != accumulatedUpdateData.arrows.end(); i++)
        {
            drawArrow(i->second);
        }

        accumulatedUpdateData.arrows.clear();

        for (auto i = accumulatedUpdateData.robots.begin(); i != accumulatedUpdateData.robots.end(); i++)
        {
            ARMARX_DEBUG << "update visu / drawRobot for robot " << i->first;
            ensureExistingRobotNodes(i->second);

            drawRobot(i->second);
        }

        accumulatedUpdateData.robots.clear();

        for (auto i = accumulatedUpdateData.coloredpointcloud.begin(); i != accumulatedUpdateData.coloredpointcloud.end(); i++)
        {
            drawColoredPointCloud(i->second);
        }

        accumulatedUpdateData.coloredpointcloud.clear();

        for (auto i = accumulatedUpdateData.colored24Bitpointcloud.begin(); i != accumulatedUpdateData.colored24Bitpointcloud.end(); i++)
        {
            draw24BitColoredPointCloud(i->second);
        }

        accumulatedUpdateData.colored24Bitpointcloud.clear();

        // check for clear and remove
        for (auto i = accumulatedUpdateData.clearLayers.begin(); i != accumulatedUpdateData.clearLayers.end(); i++)
        {
            clearLayerQt(*i);
        }
        accumulatedUpdateData.clearLayers.clear();

        for (auto i = accumulatedUpdateData.removeLayers.begin(); i != accumulatedUpdateData.removeLayers.end(); i++)
        {
            removeLayerQt(*i);
        }
        accumulatedUpdateData.removeLayers.clear();

        onUpdateVisualization();
    }

    bool DebugDrawerComponent::hasLayer(const std::string& layerName, const ::Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        return layers.find(layerName) != layers.end();
    }

    void DebugDrawerComponent::removeLayer(const std::string& layerName, const ::Ice::Current&)
    {
        ScopedRecursiveLockPtr lockData = getScopedAccumulatedDataLock();
        // check if already requested
        if (std::find(accumulatedUpdateData.removeLayers.begin(), accumulatedUpdateData.removeLayers.end(), layerName) != accumulatedUpdateData.removeLayers.end())
        {
            return;
        }

        accumulatedUpdateData.removeLayers.push_back(layerName);
    }

    void DebugDrawerComponent::removeLayerQt(const std::string& layerName)
    {
        ScopedRecursiveLockPtr lockData = getScopedAccumulatedDataLock();
        ScopedRecursiveLockPtr lockVisu = getScopedVisuLock();
        if (!hasLayer(layerName))
        {
            if (verbose)
            {
                ARMARX_VERBOSE << "Layer " << layerName << " can't be removed, because it does not exist.";
            }

            return;
        }

        if (verbose)
        {
            ARMARX_VERBOSE << "Removing layer " << layerName;
        }

        clearLayerQt(layerName);
        {
            auto& layer = layers.at(layerName);
            layerMainNode->removeChild(layer.mainNode);
            layer.mainNode->unref();
            layers.erase(layerName);
        }
    }

    void DebugDrawerComponent::removeAccumulatedData(const std::string& layerName)
    {
        ScopedRecursiveLockPtr lockData = getScopedAccumulatedDataLock();
        std::string entryName = "__" + layerName + "__";
        {
            auto i1 = accumulatedUpdateData.coord.begin();

            while (i1 != accumulatedUpdateData.coord.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.coord.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            auto i1 = accumulatedUpdateData.box.begin();

            while (i1 != accumulatedUpdateData.box.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.box.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            auto i1 = accumulatedUpdateData.line.begin();

            while (i1 != accumulatedUpdateData.line.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.line.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            auto i1 = accumulatedUpdateData.lineSet.begin();

            while (i1 != accumulatedUpdateData.lineSet.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.lineSet.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            auto i1 = accumulatedUpdateData.sphere.begin();

            while (i1 != accumulatedUpdateData.sphere.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.sphere.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            auto i1 = accumulatedUpdateData.cylinder.begin();

            while (i1 != accumulatedUpdateData.cylinder.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.cylinder.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            auto i1 = accumulatedUpdateData.text.begin();

            while (i1 != accumulatedUpdateData.text.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.text.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            auto i1 = accumulatedUpdateData.pointcloud.begin();

            while (i1 != accumulatedUpdateData.pointcloud.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.pointcloud.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            auto i1 = accumulatedUpdateData.polygons.begin();

            while (i1 != accumulatedUpdateData.polygons.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.polygons.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            auto i1 = accumulatedUpdateData.arrows.begin();

            while (i1 != accumulatedUpdateData.arrows.end())
            {
                if (boost::starts_with(i1->first, entryName))
                {
                    i1 = accumulatedUpdateData.arrows.erase(i1);
                }
                else
                {
                    i1++;
                }
            }
        }

        {
            ARMARX_DEBUG << "Removing all accumulated data for robot, nr of robots:" << accumulatedUpdateData.robots.size();

            auto i1 = accumulatedUpdateData.robots.begin();

            while (i1 != accumulatedUpdateData.robots.end())
            {
                ARMARX_DEBUG << "checking accumulated data " << i1->first;

                if (boost::starts_with(i1->first, entryName))
                {
                    ARMARX_DEBUG << "removing accumulated data for " << i1->first;
                    i1 = accumulatedUpdateData.robots.erase(i1);
                }
                else
                {
                    i1++;
                }
            }

            ARMARX_DEBUG << "end, nr of robots:" << accumulatedUpdateData.robots.size();
        }

        onRemoveAccumulatedData(layerName);

    }

    void DebugDrawerComponent::enableLayerVisu(const std::string& layerName, bool visible, const ::Ice::Current&)
    {
        setLayerVisibility(layerName, visible);
    }

    void DebugDrawerComponent::enableDebugLayerVisu(bool visible, const ::Ice::Current&)
    {
        enableLayerVisu(DEBUG_LAYER_NAME, visible);
    }

    StringSequence DebugDrawerComponent::layerNames(const ::Ice::Current&)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();
        StringSequence seq {};

        for (const auto& layer : layers)
        {
            seq.push_back(layer.first);
        }

        return seq;
    }

    ::armarx::LayerInformationSequence DebugDrawerComponent::layerInformation(const ::Ice::Current&)
    {
        ::armarx::LayerInformationSequence seq {};
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        for (const auto& layer : layers)
        {
            int count = layer.second.addedCoordVisualizations.size() +
                        layer.second.addedLineVisualizations.size() +
                        layer.second.addedLineSetVisualizations.size() +
                        layer.second.addedBoxVisualizations.size() +
                        layer.second.addedTextVisualizations.size() +
                        layer.second.addedSphereVisualizations.size() +
                        layer.second.addedCylinderVisualizations.size() +
                        layer.second.addedPointCloudVisualizations.size() +
                        layer.second.addedArrowVisualizations.size() +
                        layer.second.addedRobotVisualizations.size() +
                        layer.second.addedCustomVisualizations.size();
            ::armarx::LayerInformation info = {layer.first, layer.second.visible, count};
            seq.push_back(info);
        }

        return seq;
    }

    void DebugDrawerComponent::drawColoredPointCloud(const ColoredPointCloudData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        auto& layer = requestLayer(d.layerName);

        removeColoredPointCloud(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        const auto& pcl = d.pointCloud.points;

        SoSeparator* pclSep = new SoSeparator;

        SoMaterial* pclMat = new SoMaterial;
        std::vector<SbColor> colors;
        colors.reserve(pcl.size());
        std::transform(
            pcl.begin(), pcl.end(), std::back_inserter(colors),
            [](const DebugDrawerColoredPointCloudElement & elem)
        {
            return SbColor {elem.color.r, elem.color.g, elem.color.b};
        }
        );
        pclMat->diffuseColor.setValues(0, colors.size(), colors.data());
        pclMat->ambientColor.setValues(0, colors.size(), colors.data());
        pclSep->addChild(pclMat);

        SoMaterialBinding* pclMatBind = new SoMaterialBinding;
        pclMatBind->value = SoMaterialBinding::PER_PART;
        pclSep->addChild(pclMatBind);

        SoCoordinate3* pclCoords = new SoCoordinate3;
        std::vector<SbVec3f> coords;
        coords.reserve(pcl.size());
        std::transform(
            pcl.begin(), pcl.end(), std::back_inserter(coords),
            [](const DebugDrawerColoredPointCloudElement & elem)
        {
            return SbVec3f {elem.x, elem.y, elem.z};
        }
        );
        pclCoords->point.setValues(0, coords.size(), coords.data());
        pclSep->addChild(pclCoords);

        SoDrawStyle* pclStye = new SoDrawStyle;
        pclStye->pointSize = d.pointCloud.pointSize;
        pclSep->addChild(pclStye);

        pclSep->addChild(new SoPointSet);

        ARMARX_INFO << d.name << "PointCloud Update " << pcl.size();

        layer.addedColoredPointCloudVisualizations[d.name] = pclSep;
        layer.mainNode->addChild(pclSep);
    }

    void DebugDrawerComponent::removeColoredPointCloud(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.addedColoredPointCloudVisualizations.find(name) == layer.addedColoredPointCloudVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.addedColoredPointCloudVisualizations[name]);
        layer.addedColoredPointCloudVisualizations.erase(name);
    }

    void DebugDrawerComponent::setColoredPointCloudVisu(const std::string& layerName, const std::string& pointCloudName, const DebugDrawerColoredPointCloud& pointCloud, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + pointCloudName + "__";
            ColoredPointCloudData& d = accumulatedUpdateData.coloredpointcloud[entryName];
            d.pointCloud = pointCloud;
            d.layerName = layerName;
            d.name = pointCloudName;
            d.active = true;
        }
    }

    void DebugDrawerComponent::setColoredPointCloudDebugLayerVisu(const std::string& pointCloudName, const DebugDrawerColoredPointCloud& pointCloud, const Ice::Current&)
    {
        setColoredPointCloudVisu(DEBUG_LAYER_NAME, pointCloudName, pointCloud);
    }

    void DebugDrawerComponent::removeColoredPointCloudVisu(const std::string& layerName, const std::string& pointCloudName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + pointCloudName + "__";
            ColoredPointCloudData& d = accumulatedUpdateData.coloredpointcloud[entryName];
            d.layerName = layerName;
            d.name = pointCloudName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::removeColoredPointCloudDebugLayerVisu(const std::string& pointCloudName, const Ice::Current&)
    {
        removeColoredPointCloudVisu(DEBUG_LAYER_NAME, pointCloudName);
    }

    void DebugDrawerComponent::draw24BitColoredPointCloud(const Colored24BitPointCloudData& d)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        auto& layer = requestLayer(d.layerName);

        remove24BitColoredPointCloud(d.layerName, d.name);

        if (!d.active)
        {
            return;
        }

        const auto& pcl = d.pointCloud.points;

        SoSeparator* pclSep = new SoSeparator;

        SoMaterial* pclMat = new SoMaterial;
        std::vector<SbColor> colors;
        colors.reserve(pcl.size());
        std::transform(
            pcl.begin(), pcl.end(), std::back_inserter(colors),
            [](const DebugDrawer24BitColoredPointCloudElement & elem)
        {
            return SbColor {static_cast<float>(elem.color.r) / 255.f, static_cast<float>(elem.color.g) / 255.f, static_cast<float>(elem.color.b) / 255.f};
        }
        );
        pclMat->diffuseColor.setValues(0, colors.size(), colors.data());
        pclMat->ambientColor.setValues(0, colors.size(), colors.data());
        pclSep->addChild(pclMat);

        SoMaterialBinding* pclMatBind = new SoMaterialBinding;
        pclMatBind->value = SoMaterialBinding::PER_PART;
        pclSep->addChild(pclMatBind);

        SoCoordinate3* pclCoords = new SoCoordinate3;
        std::vector<SbVec3f> coords;
        coords.reserve(pcl.size());
        std::transform(
            pcl.begin(), pcl.end(), std::back_inserter(coords),
            [](const DebugDrawer24BitColoredPointCloudElement & elem)
        {
            return SbVec3f {elem.x, elem.y, elem.z};
        }
        );
        pclCoords->point.setValues(0, coords.size(), coords.data());
        pclSep->addChild(pclCoords);

        SoDrawStyle* pclStye = new SoDrawStyle;
        pclStye->pointSize = d.pointCloud.pointSize;
        pclSep->addChild(pclStye);

        pclSep->addChild(new SoPointSet);

        ARMARX_INFO << d.name << "PointCloud Update " << pcl.size();

        layer.added24BitColoredPointCloudVisualizations[d.name] = pclSep;
        layer.mainNode->addChild(pclSep);
    }

    void DebugDrawerComponent::remove24BitColoredPointCloud(const std::string& layerName, const std::string& name)
    {
        ScopedRecursiveLockPtr l = getScopedVisuLock();

        if (!hasLayer(layerName))
        {
            return;
        }

        auto& layer = layers.at(layerName);

        if (layer.added24BitColoredPointCloudVisualizations.find(name) == layer.added24BitColoredPointCloudVisualizations.end())
        {
            return;
        }

        layer.mainNode->removeChild(layer.added24BitColoredPointCloudVisualizations[name]);
        layer.added24BitColoredPointCloudVisualizations.erase(name);
    }

    void DebugDrawerComponent::set24BitColoredPointCloudVisu(const std::string& layerName, const std::string& pointCloudName, const DebugDrawer24BitColoredPointCloud& pointCloud, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + pointCloudName + "__";
            Colored24BitPointCloudData& d = accumulatedUpdateData.colored24Bitpointcloud[entryName];
            d.pointCloud = pointCloud;
            d.layerName = layerName;
            d.name = pointCloudName;
            d.active = true;
        }
    }

    void DebugDrawerComponent::set24BitColoredPointCloudDebugLayerVisu(const std::string& pointCloudName, const DebugDrawer24BitColoredPointCloud& pointCloud, const Ice::Current&)
    {
        set24BitColoredPointCloudVisu(DEBUG_LAYER_NAME, pointCloudName, pointCloud);
    }

    void DebugDrawerComponent::remove24BitColoredPointCloudVisu(const std::string& layerName, const std::string& pointCloudName, const Ice::Current&)
    {
        {
            ScopedRecursiveLockPtr l = getScopedAccumulatedDataLock();
            std::string entryName = "__" + layerName + "__" + pointCloudName + "__";
            Colored24BitPointCloudData& d = accumulatedUpdateData.colored24Bitpointcloud[entryName];
            d.layerName = layerName;
            d.name = pointCloudName;
            d.active = false;
        }
    }

    void DebugDrawerComponent::remove24BitColoredPointCloudDebugLayerVisu(const std::string& pointCloudName, const Ice::Current&)
    {
        remove24BitColoredPointCloudVisu(DEBUG_LAYER_NAME, pointCloudName);
    }

}//namespace armarx
