/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Peter Kaiser (peter dot kaiser at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "BusInspectionUnitObserver.h"

#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>
#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckInRange.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/checks/ConditionCheckEqualsWithTolerance.h>
#include <RobotAPI/libraries/core/checks/ConditionCheckMagnitudeChecks.h>
#include <RobotAPI/libraries/core/observerfilters/OffsetFilter.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <RobotAPI/libraries/core/RobotAPIObjectFactories.h>

using namespace armarx;

BusInspectionUnitObserver::BusInspectionUnitObserver()
{
}

void BusInspectionUnitObserver::setTopicName(std::string topicName)
{
    this->topicName = topicName;
}

void BusInspectionUnitObserver::onInitObserver()
{
    if (topicName.empty())
    {
        usingTopic(getProperty<std::string>("BusInspectionUnitTopicName").getValue());
        ARMARX_INFO << "Using topic " << getProperty<std::string>("BusInspectionUnitTopicName").getValue();
    }
    else
    {
        usingTopic(topicName);
        ARMARX_INFO << "Using topic " << topicName;
    }
}

void BusInspectionUnitObserver::onConnectObserver()
{
}

PropertyDefinitionsPtr BusInspectionUnitObserver::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new BusInspectionUnitObserverPropertyDefinitions(getConfigIdentifier()));
}

void BusInspectionUnitObserver::reportCycleTimeStatistics(int averageCycleTime, int minCycleTime, int maxCycleTime, const Ice::Current&)
{
    ScopedLock lock(dataMutex);

    try
    {
        std::string channelName = "HALCycleTimes";

        DataFieldIdentifierPtr id_avg = new DataFieldIdentifier(getName(), channelName, "Average HAL Cycle");
        if (!existsChannel(id_avg->channelName))
        {
            offerChannel(id_avg->channelName, "HAL Cycle times");
        }

        if (!existsDataField(id_avg->channelName, id_avg->datafieldName))
        {
            offerDataFieldWithDefault(id_avg->channelName, id_avg->datafieldName, Variant(averageCycleTime), "Average HAL cycle time");
        }
        else
        {
            setDataField(id_avg->channelName, id_avg->datafieldName, Variant(averageCycleTime));
        }

        DataFieldIdentifierPtr id_min = new DataFieldIdentifier(getName(), channelName, "Minimum HAL Cycle");
        if (!existsDataField(id_min->channelName, id_min->datafieldName))
        {
            offerDataFieldWithDefault(id_min->channelName, id_min->datafieldName, Variant(minCycleTime), "Minimum HAL cycle time");
        }
        else
        {
            setDataField(id_min->channelName, id_min->datafieldName, Variant(minCycleTime));
        }

        DataFieldIdentifierPtr id_max = new DataFieldIdentifier(getName(), channelName, "Maximum HAL Cycle");
        if (!existsDataField(id_max->channelName, id_max->datafieldName))
        {
            offerDataFieldWithDefault(id_max->channelName, id_max->datafieldName, Variant(maxCycleTime), "Maximum HAL cycle time");
        }
        else
        {
            setDataField(id_max->channelName, id_max->datafieldName, Variant(maxCycleTime));
        }

        updateChannel(channelName);
    }
    catch (std::exception& e)
    {
        ARMARX_ERROR << "Reporting HAL cycle time failed! ";
        handleExceptions();
    }
}
