/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Peter Kaiser (peter dot kaiser at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef _ARMARX_ROBOTAPI_BUSINSPECTIONUNITOBSERVER_H
#define _ARMARX_ROBOTAPI_BUSINSPECTIONUNITOBSERVER_H

#include <RobotAPI/interface/hardware/BusInspectionInterface.h>
#include <ArmarXCore/observers/Observer.h>

namespace armarx
{
    /**
     * \class ForceTorqueObserverPropertyDefinitions
     * \brief
     */
    class BusInspectionUnitObserverPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        BusInspectionUnitObserverPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("BusInspectionUnitTopicName", "HALCycleTimes", "Name of the BusInspectionUnit Topic");
        }
    };

    class BusInspectionUnitObserver :
        virtual public Observer,
        virtual public BusInspectionUnitObserverInterface
    {
    public:
        BusInspectionUnitObserver();

        void setTopicName(std::string topicName);

        virtual std::string getDefaultName() const
        {
            return "BusInspectionUnitObserver";
        }
        void onInitObserver();
        void onConnectObserver();

        virtual void reportCycleTimeStatistics(int averageCycleTime, int minCycleTime, int maxCycleTime, const Ice::Current&);

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    private:
        armarx::Mutex dataMutex;
        std::string topicName;
    };
}

#endif
