/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef ARMARX_COMPONENT_FORCE_TORQUE_UNIT_SIMULATION_H
#define ARMARX_COMPONENT_FORCE_TORQUE_UNIT_SIMULATION_H

#include "ForceTorqueUnit.h"

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <RobotAPI/libraries/core/FramedPose.h>

#include <IceUtil/Time.h>

#include <string>

namespace armarx
{
    /**
     * \class ForceTorqueUnitSimulationPropertyDefinitions
     * \brief
     */
    class ForceTorqueUnitSimulationPropertyDefinitions :
        public ForceTorqueUnitPropertyDefinitions
    {
    public:
        ForceTorqueUnitSimulationPropertyDefinitions(std::string prefix) :
            ForceTorqueUnitPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("SensorName", "simulated sensor name");
            defineOptionalProperty<int>("IntervalMs", 10, "The time in milliseconds between two calls to the simulation method.");
        }
    };

    /**
     * \class ForceTorqueUnitSimulation
     * \brief Simulates a set of Force/Torque sensors.
     * \ingroup RobotAPI-SensorActorUnits-simulation
     *
     * The unit is given a list of sensor names as a property. It then publishes force/torque values under these names.
     * The published values will always be zero.
     */
    class ForceTorqueUnitSimulation :
        virtual public ForceTorqueUnit
    {
    public:
        virtual std::string getDefaultName() const
        {
            return "ForceTorqueUnitSimulation";
        }

        virtual void onInitForceTorqueUnit();
        virtual void onStartForceTorqueUnit();
        virtual void onExitForceTorqueUnit();

        void simulationFunction();

        /**
         * \warning Not implemented yet
         */
        virtual void setOffset(const FramedDirectionBasePtr& forceOffsets, const FramedDirectionBasePtr& torqueOffsets, const Ice::Current& c = ::Ice::Current());

        /**
         * \warning Not implemented yet
         */
        virtual void setToNull(const Ice::Current& c = ::Ice::Current());

        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    protected:
        armarx::FramedDirectionPtr forces;
        armarx::FramedDirectionPtr torques;

        PeriodicTask<ForceTorqueUnitSimulation>::pointer_type simulationTask;
    };
}

#endif
