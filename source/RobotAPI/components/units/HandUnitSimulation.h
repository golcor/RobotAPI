/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     David Schiebener (schiebener at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_UNITS_HANDUNITSIMULATION_H
#define _ARMARX_CORE_UNITS_HANDUNITSIMULATION_H


#include "HandUnit.h"

namespace armarx
{
    /**
     * \class HandUnitSimulationPropertyDefinitions
     * \brief Defines all necessary properties for armarx::HandUnitSimulation
     */
    class HandUnitSimulationPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        HandUnitSimulationPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("RobotFileName", "VirtualRobot XML file in which the endeffector is is stored.");
            defineRequiredProperty<std::string>("EndeffectorName", "Name of the endeffector as stored in the XML file (will publish values on EndeffectorName + 'State')");
        }
    };


    /**
     * \class HandUnitSimulation
     * \ingroup RobotAPI-SensorActorUnits-simulation
     * \brief Simulates a robot hand.
     *
     * An instance of a HandUnitSimulation provides means to open, close, and shape hands.
     * It uses the HandUnitListener Ice interface to report updates of its current state
     *
     * \warning This component is not yet fully functional
     */
    class HandUnitSimulation :
        virtual public HandUnit
    {
    public:
        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "HandUnitSimulation";
        }

        virtual void onInitHandUnit();
        virtual void onStartHandUnit();
        virtual void onExitHandUnit();

        /**
         * Send command to the hand to form a specific shape position.
         *
         * \warning Not implemented yet!
         */
        virtual void setShape(const std::string& shapeName, const Ice::Current& c = ::Ice::Current());

        /**
         * \warning Not implemented yet!
         */
        virtual void setJointAngles(const NameValueMap& jointAngles, const Ice::Current& c = ::Ice::Current());
    };
}

#endif
