/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Peter Kaiser <peter dot kaiser at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_HAPTIC_UNIT_H
#define _ARMARX_COMPONENT_HAPTIC_UNIT_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include "RobotAPI/components/units/SensorActorUnit.h"

#include <RobotAPI/interface/units/HapticUnit.h>

#include <string>

namespace armarx
{
    /**
     * \class HapticUnitPropertyDefinitions
     * \brief
     */
    class HapticUnitPropertyDefinitions : public ComponentPropertyDefinitions
    {
    public:
        HapticUnitPropertyDefinitions(std::string prefix) :
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("HapticTopicName", "HapticValues", "Name of the Haptic Topic.");
        }
    };

    /**
     * \defgroup Component-HapticUnit HapticUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Base unit for haptic sensors.
     */

    /**
     * @ingroup Component-HapticUnit
     * @brief The HapticUnit class
     */
    class HapticUnit :
        virtual public HapticUnitInterface,
        virtual public SensorActorUnit
    {
    public:
        virtual std::string getDefaultName() const
        {
            return "HapticUnit";
        }

        virtual void onInitComponent();
        virtual void onConnectComponent();
        virtual void onExitComponent();

        virtual void onInitHapticUnit() = 0;
        virtual void onStartHapticUnit() = 0;
        virtual void onExitHapticUnit() = 0;

        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    protected:
        HapticUnitListenerPrx hapticTopicPrx;
    };
}

#endif
