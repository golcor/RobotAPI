/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Christian Boege (boege dot at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_KINEMATIC_UNIT_SIMULATION_H
#define _ARMARX_COMPONENT_KINEMATIC_UNIT_SIMULATION_H

#include "KinematicUnit.h"

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <random>

#include <IceUtil/Time.h>

namespace armarx
{
    /**
     * \class KinematicUnitSimulationJointState.
     * \brief State of a joint.
     * \ingroup RobotAPI-SensorActorUnits-util
     *
     * Includes the control mode, the joint angle, velocity, torque, and motor temperature.
     * Controlmode defaults to ePositionControl, all other values default to zero.
     */
    class KinematicUnitSimulationJointState
    {
    public:
        KinematicUnitSimulationJointState()
        {
            init();
        }

        void init()
        {
            controlMode = eDisabled;
            angle = 0.0f;
            velocity = 0.0f;
            torque = 0.0f;
            temperature = 0.0f;
        }

        ControlMode controlMode;
        float angle;
        float velocity;
        float torque;
        float temperature;
    };
    typedef std::map<std::string, KinematicUnitSimulationJointState> KinematicUnitSimulationJointStates;

    /**
     * \class JointInfo.
     * \brief Additional information about a joint.
     * \ingroup RobotAPI-SensorActorUnits-util
     *
     * Joint info including lower and upper joint limits (rad).
     * No limits are set by default (lo = hi).
     */
    class KinematicUnitSimulationJointInfo
    {
    public:
        KinematicUnitSimulationJointInfo()
        {
            reset();
        }

        void reset()
        {
            limitLo = 0.0f;
            limitHi = 0.0f;
        }
        bool hasLimits() const
        {
            return (limitLo != limitHi);
        }

        float limitLo;
        float limitHi;
    };
    typedef std::map<std::string, KinematicUnitSimulationJointInfo> KinematicUnitSimulationJointInfos;

    /**
     * \class KinematicUnitSimulationPropertyDefinitions
     * \brief
     */
    class KinematicUnitSimulationPropertyDefinitions :
        public KinematicUnitPropertyDefinitions
    {
    public:
        KinematicUnitSimulationPropertyDefinitions(std::string prefix) :
            KinematicUnitPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("IntervalMs", 10, "The time in milliseconds between two calls to the simulation method.");
            defineOptionalProperty<float>("Noise", 0.0f, "Noise is added in velocity mode with (rand()/RAND_MAX-0.5)*2*Noise. Value in Degree");
        }
    };

    /**
     * \class KinematicUnitSimulation
     * \brief Simulates robot kinematics
     * \ingroup RobotAPI-SensorActorUnits-simulation
     */
    class KinematicUnitSimulation :
        virtual public KinematicUnit
    {
    public:
        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "KinematicUnitSimulation";
        }

        virtual void onInitKinematicUnit();
        virtual void onStartKinematicUnit();
        virtual void onExitKinematicUnit();

        void simulationFunction();

        // proxy implementation
        virtual void requestJoints(const StringSequence& joints, const Ice::Current& c = ::Ice::Current());
        virtual void releaseJoints(const StringSequence& joints, const Ice::Current& c = ::Ice::Current());
        virtual void switchControlMode(const NameControlModeMap& targetJointModes, const Ice::Current& c = ::Ice::Current());
        virtual void setJointAngles(const NameValueMap& targetJointAngles, const Ice::Current& c = ::Ice::Current());
        virtual void setJointVelocities(const NameValueMap& targetJointVelocities, const Ice::Current& c = ::Ice::Current());
        virtual void setJointTorques(const NameValueMap& targetJointTorques, const Ice::Current& c = ::Ice::Current());

        virtual NameControlModeMap getControlModes(const Ice::Current& c = ::Ice::Current());

        /**
         * \warning Not implemented yet!
         */
        virtual void setJointAccelerations(const NameValueMap& targetJointAccelerations, const Ice::Current& c = ::Ice::Current());

        /**
         * \warning Not implemented yet!
         */
        virtual void setJointDecelerations(const NameValueMap& targetJointDecelerations, const Ice::Current& c = ::Ice::Current());

        void stop(const Ice::Current& c = Ice::Current());

        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    protected:
        KinematicUnitSimulationJointStates jointStates;
        KinematicUnitSimulationJointStates previousJointStates;
        KinematicUnitSimulationJointInfos jointInfos;
        boost::mutex jointStatesMutex;
        IceUtil::Time lastExecutionTime;
        float noise;
        int intervalMs;
        std::normal_distribution<double> distribution;
        std::default_random_engine generator;

        PeriodicTask<KinematicUnitSimulation>::pointer_type simulationTask;
    };
}

#endif
