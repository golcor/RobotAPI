/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::units
 * @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_CORE_UNITS_PLATFROMUNIT_H
#define _ARMARX_CORE_UNITS_PLATFROMUNIT_H

#include <RobotAPI/components/units/SensorActorUnit.h>

#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <RobotAPI/interface/units/PlatformUnitInterface.h>

#include <vector>

namespace armarx
{
    /**
     * \class PlatformUnitPropertyDefinitions
     * \brief Defines all necessary properties for armarx::PlatformUnit
     */
    class PlatformUnitPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        PlatformUnitPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PlatformName", "Platform", "Name of the platform (will publish values on PlatformName + 'State')");
        }
    };


    /**
     * \defgroup Component-PlatformUnit PlatformUnit
     * \ingroup RobotAPI-SensorActorUnits
     * \brief Base unit for high-level access to robot platforms.
     *
     * This class defines an interface for providing high level access to robot platforms
     * An instance of a PlatformUnit provides means to set target positions.
     * It uses the PlatformUnitListener Ice interface to report updates of its current state.
     */

    /**
     * @ingroup Component-PlatformUnit
     * @brief The PlatformUnit class
     */
    class PlatformUnit :
        virtual public PlatformUnitInterface,
        virtual public SensorActorUnit
    {
    public:
        // inherited from Component
        virtual std::string getDefaultName() const
        {
            return "PlatformUnit";
        }

        /**
         * Retrieve proxy for publishing State information and call
         * armarx::PlatformUnit::onInitPlatformUnit().
         * \see armarx::Component::onInitComponent()
         */
        virtual void onInitComponent();
        /**
         * Calls armarx::PlatformUnit::onStartPlatformUnit().
         * \see armarx::Component::onConnectComponent()
         */
        virtual void onConnectComponent();

        virtual void onDisconnectComponent();
        /**
         * Calls armarx::PlatformUnit::onExitPlatformUnit().
         * \see armarx::Component::onExitComponent()
         */
        virtual void onExitComponent();

        virtual void onInitPlatformUnit() = 0;
        virtual void onStartPlatformUnit() = 0;
        virtual void onStopPlatformUnit() {}
        virtual void onExitPlatformUnit() = 0;

        /**
         * Set a new target position and orientation for the platform.
         * The platform will move until it reaches the specified target with the specified accuracy.
         */
        virtual void moveTo(Ice::Float targetPlatformPositionX, Ice::Float targetPlatformPositionY, Ice::Float targetPlatformRotation,
                            Ice::Float positionalAccuracy, Ice::Float orientationalAccuracy, const Ice::Current& c = ::Ice::Current());

        void stopPlatform(const Ice::Current& c = Ice::Current()) {}
        /**
         * \see armarx::PropertyUser::createPropertyDefinitions()
         */
        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    protected:
        /**
         * PlatformUnitListener proxy for publishing state updates
         */
        PlatformUnitListenerPrx listenerPrx;
        /**
         * Ice Topic name on which armarx::PlatformUnit::listenerPrx publishes information
         */
        std::string listenerChannelName;
    };
}

#endif
