#ifndef ABSTRACTINTERFACE_H
#define ABSTRACTINTERFACE_H

#include <string>
#include "Types.h"
#include "BinaryLogger.h"
#include <boost/shared_ptr.hpp>


#define MSG_PREAMBLE_BYTE       0xaa
#define MSG_PREAMBLE_LEN        3

// Combine bytes to different types
#define make_short( lowbyte, highbyte )             ( (unsigned short)lowbyte | ( (unsigned short)highbyte << 8 ) )
#define make_signed_short( lowbyte, highbyte )      ( (signed short) ( (unsigned short) lowbyte | ( (unsigned short) highbyte << 8 ) ) )
#define make_int( lowbyte, mid1, mid2, highbyte )   ( (unsigned int) lowbyte | ( (unsigned int) mid1 << 8 ) | ( (unsigned int) mid2 << 16 ) | ( (unsigned int) highbyte << 24 ) )
#define make_float( result, byteptr )               memcpy( &result, byteptr, sizeof( float ) )

// Byte access
#define hi( x )     (unsigned char) ( ((x) >> 8) & 0xff )   // Returns the upper byte of the passed short
#define lo( x )     (unsigned char) ( (x) & 0xff )          // Returns the lower byte of the passed short



struct Response;

class AbstractInterface
{
public:
    AbstractInterface();
    virtual ~AbstractInterface();
    virtual int open() = 0;
    virtual void close() = 0;
    int read(unsigned char* buf, unsigned int len);
    int write(unsigned char* buf, unsigned int len);

    bool IsConnected() const
    {
        return connected;
    }

    virtual std::string toString() const = 0;

    int send(unsigned char id, unsigned int len, unsigned char* data);
    int receive(msg_t* msg);
    Response submitCmd(unsigned char id, unsigned char* payload, unsigned int len, bool pending);
    Response receive(bool pending, unsigned char expectedId);
    Response receiveWithoutChecks();
    void fireAndForgetCmd(unsigned char id, unsigned char* payload, unsigned int len, bool pending);

    void startLogging(std::string file);
    void logText(std::string message);

protected:
    bool connected;

    virtual int readInternal(unsigned char* buf, unsigned int len) = 0;
    virtual int writeInternal(unsigned char* buf, unsigned int len) = 0;

private:
    friend std::ostream& operator<<(std::ostream&, const AbstractInterface&);
    boost::shared_ptr<BinaryLogger> log;
};

std::ostream& operator<<(std::ostream& strm, const AbstractInterface& a);


#endif // ABSTRACTINTERFACE_H
