#include "BinaryLogger.h"
#include <boost/format.hpp>

BinaryLogger::BinaryLogger(std::string filename)
{
    this->log.open(filename.c_str());
}

BinaryLogger::~BinaryLogger()
{
    log.close();
}

void BinaryLogger::logRead(unsigned char* buf, unsigned int len)
{
    log << "READ";

    for (unsigned int i = 0; i < len; i++)
    {
        log << boost::format(" %02X") % (int)buf[i];
    }

    log << std::endl;
    log.flush();
}

void BinaryLogger::logWrite(unsigned char* buf, unsigned int len)
{
    log << "WRITE";

    for (unsigned int i = 0; i < len; i++)
    {
        log << boost::format(" %02X") % (int)buf[i];
    }

    log << std::endl;
    log.flush();
}

void BinaryLogger::logText(std::string message)
{
    log << message << std::endl;
    log.flush();
}
