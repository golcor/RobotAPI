armarx_set_target("WeissHapticSensor Library: WeissHapticSensor")

find_package(Eigen3 QUIET)

armarx_build_if(Eigen3_FOUND "Eigen3 not available")

if (Eigen3_FOUND)
    include_directories(
        ${Eigen3_INCLUDE_DIR})
endif()

set(LIB_NAME       WeissHapticSensor)



set(LIBS RobotAPIUnits ArmarXCoreObservers ArmarXCoreEigen3Variants)

set(LIB_FILES
    WeissHapticUnit.cpp
    WeissHapticSensor.cpp
    AbstractInterface.cpp
    BinaryLogger.cpp
    TextWriter.cpp
    Checksum.cpp
    SerialInterface.cpp
    TactileSensor.cpp
    CalibrationInfo.cpp
    CalibrationHelper.cpp
)
set(LIB_HEADERS
    WeissHapticUnit.h
    WeissHapticSensor.h
    AbstractInterface.h
    BinaryLogger.h
    TextWriter.h
    Checksum.h
    Response.h
    SerialInterface.h
    TactileSensor.h
    TransmissionException.h
    Types.h
    CalibrationInfo.h
    CalibrationHelper.h
)

armarx_add_library("${LIB_NAME}"  "${LIB_FILES}" "${LIB_HEADERS}" "${LIBS}")
