#ifndef CALIBRATIONHELPER_H
#define CALIBRATIONHELPER_H

#include <vector>
#include <Eigen/Core>

#include "CalibrationInfo.h"

namespace armarx
{
    class CalibrationHelper
    {
    public:
        CalibrationHelper(int rows, int cols, float noiseThreshold);

        void addNoiseSample(Eigen::MatrixXf data);
        bool addMaxValueSample(Eigen::MatrixXf data);

        CalibrationInfo getCalibrationInfo(float calibratedMinimum, float calibratedMaximum);

        bool checkMaximumValueThreshold(float threshold);

        Eigen::MatrixXf getMaximumValues();

        int getNoiseSampleCount();

    private:
        std::vector<Eigen::MatrixXf> noiseSamples;
        Eigen::MatrixXf maximumValues;
        float noiseThreshold;

        Eigen::MatrixXf getMatrixAverage(std::vector<Eigen::MatrixXf> samples);
    };
}

#endif // CALIBRATIONHELPER_H
