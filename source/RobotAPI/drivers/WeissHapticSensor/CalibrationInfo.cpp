#include "CalibrationInfo.h"

using namespace armarx;

CalibrationInfo::CalibrationInfo(Eigen::MatrixXf averageNoiseValues, Eigen::MatrixXf maximumValues, float minValue, float maxValue)
{
    this->calibratedMinimum = minValue;
    this->calibratedMaximum = maxValue;
    this->averageNoiseValues = averageNoiseValues;
    this->maximumValues = maximumValues;
}

Eigen::MatrixXf CalibrationInfo::applyCalibration(Eigen::MatrixXf rawData)
{
    return ((rawData - averageNoiseValues).cwiseQuotient(maximumValues - averageNoiseValues).array() * (calibratedMaximum - calibratedMinimum) + calibratedMinimum).matrix();
}
