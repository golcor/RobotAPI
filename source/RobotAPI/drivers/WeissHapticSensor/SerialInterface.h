#ifndef SERIALINTERFACE_H
#define SERIALINTERFACE_H

#include "AbstractInterface.h"
#include <iostream>


class SerialInterface : public AbstractInterface
{
public:
    SerialInterface(const char* device, const unsigned int bitrate);
    virtual ~SerialInterface();

    virtual int open();
    virtual void close();

    virtual std::string toString() const;

protected:
    virtual int readInternal(unsigned char*, unsigned int);
    virtual int writeInternal(unsigned char*, unsigned int);

private:
    const char* device;
    unsigned int bitrate;
    int fd;

    int blockingReadAll(unsigned char* buf, unsigned int len);
};

#endif // SERIALINTERFACE_H
