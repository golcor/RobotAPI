#ifndef TACTILESENSOR_H
#define TACTILESENSOR_H

#include "AbstractInterface.h"
using namespace std;
#include <boost/shared_ptr.hpp>

#define extract_short( array, index )               ( (unsigned short)array[index] | ( (unsigned short)array[index + 1] << 8 ) )
#define TAC_CHECK_RES( res, expected, resp )    { \
        if ( res < expected ) { \
            dbgPrint( "Response length is too short, should be = %d (is %d)\n", expected, res ); \
            if ( res > 0 ) free( resp ); \
            return -1; \
        } \
        status_t status = cmd_get_response_status( resp ); \
        if ( status != E_SUCCESS ) \
        { \
            dbgPrint( "Command not successful: %s\n", status_to_str( status ) ); \
            free( resp ); \
            return -1; \
        } \
    }

typedef struct
{
    int res_x; // Horizontal matrix resolution. For non-rectangular matrices, this is the horizontal resolution of the surrounding rectangle measured in sensor cells.
    int res_y; // Vertical matrix resolution. For non-rectangular matrices, this is the vertical resolution of the surrounding rectangle measured in sensor cells.
    int cell_width; // Width of one sensor cell in 1/100 millimeters.
    int cell_height; // Height of one sensor cell in 1/100 millimeters.
    int fullscale; // Full scale value of the output signal.
} tac_matrix_info_t;

typedef struct
{
    unsigned char type; // System Type: 0 - unknown, 4 - WTS Tactile Sensor Module
    unsigned char hw_rev; // Hardware Revision
    unsigned short fw_version; // Firmware Version: D15...12: major version, D11...8: minor version 1, D7..4 minor version 2, D3..0: 0 for release version, 1..15 for release candidate versions
    unsigned short sn; // Serial Number of the device
} tac_system_information_t;

struct FrameData
{
public:
    FrameData(boost::shared_ptr<std::vector<short> > data, int count)
        : data(data), count(count)
    {}
    boost::shared_ptr<std::vector<short> > data;
    int count;
};
struct PeriodicFrameData
{
public:
    PeriodicFrameData(boost::shared_ptr<std::vector<short> > data, int count, unsigned int timestamp)
        : data(data), count(count), timestamp(timestamp)
    {}
    boost::shared_ptr<std::vector<short> > data;
    int count;
    unsigned int timestamp;
};

class TactileSensor
{
public:
    TactileSensor(boost::shared_ptr<AbstractInterface> interface);
    virtual ~TactileSensor();

    tac_matrix_info_t getMatrixInformation();
    static void printMatrixInfo(tac_matrix_info_t* mi);
    FrameData readSingleFrame();
    static void printMatrix(short* matrix, int width, int height);

    void startPeriodicFrameAcquisition(unsigned short delay_ms);
    void stopPeriodicFrameAcquisition(void);
    PeriodicFrameData receicePeriodicFrame();
    void tareSensorMatrix(unsigned char operation);
    void setAquisitionWindow(unsigned char x1, unsigned char y1, unsigned char x2, unsigned char y2);
    int setAdvanvedAcquisitionMask(char* mask);
    int getAcquisitionMask(char** mask, int* mask_len);
    void setThreshold(short threshold);
    unsigned short getThreshold();
    void setFrontEndGain(unsigned char gain);
    unsigned char getFrontEndGain();
    string getSensorType();
    float readDeviceTemperature();
    tac_system_information_t getSystemInformation();
    static void printSystemInformation(tac_system_information_t si);
    void setDeviceTag(string tag);
    string getDeviceTag();
    bool tryGetDeviceTag(string& tag);
    int loop(char* data, int data_len);

    string getInterfaceInfo();

private:
    boost::shared_ptr<AbstractInterface> interface;
    FrameData getFrameData(Response* response);
    PeriodicFrameData getPeriodicFrameData(Response* response);

private:
    friend std::ostream& operator<<(std::ostream&, const TactileSensor&);
};

std::ostream& operator<<(std::ostream& strm, const TactileSensor& a);

#endif // TACTILESENSOR_H
