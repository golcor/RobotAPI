#ifndef TRANSMISSIONEXCEPTION_H
#define TRANSMISSIONEXCEPTION_H

#include <stdexcept>

class TransmissionException : public std::runtime_error
{
public:
    TransmissionException(std::string message)
        : runtime_error(message)
    { }
};

class ChecksumErrorException : public TransmissionException
{
public:
    ChecksumErrorException(std::string message)
        : TransmissionException(message)
    { }
};

#endif // TRANSMISSIONEXCEPTION_H
