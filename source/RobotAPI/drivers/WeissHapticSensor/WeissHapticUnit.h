/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Armar4::units
 * @author     Simon Ottenhaus <simon dot ottenhaus at kit dot edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef ARMAR4_FT_UNIT_ARMAR4_H
#define ARMAR4_FT_UNIT_ARMAR4_H

#include <RobotAPI/components/units/HapticUnit.h>
#include <RobotAPI/interface/units/WeissHapticUnit.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>

#include <string>
#include "WeissHapticSensor.h"

namespace armarx
{
    class WeissHapticUnitPropertyDefinitions : public HapticUnitPropertyDefinitions
    {
    public:
        WeissHapticUnitPropertyDefinitions(std::string prefix):
            HapticUnitPropertyDefinitions(prefix)
        {
        }
    };

    class WeissHapticUnit :
        virtual public WeissHapticUnitInterface,
        virtual public HapticUnit
    {
    public:
        virtual std::string getDefaultName()
        {
            return "WeissHapticUnit";
        }

        virtual void onInitHapticUnit();
        virtual void onStartHapticUnit();
        virtual void onExitHapticUnit();

        //virtual void onConnectComponent();
        virtual void onDisconnectComponent();

        virtual PropertyDefinitionsPtr createPropertyDefinitions();

    protected:
        //void proceedSensorCategory(SensorCategoryDefinition<MatrixFloat> *category);

        //std::map<std::string, MatrixFloatPtr> currentValues;



        //HapticSensorProtocolMaster hapticProtocol;

        //bool remoteSystemReady;

    private:
        std::vector< std::string > getDevices();

        std::vector< boost::shared_ptr< WeissHapticSensor > > sensors;

        // WeissHapticUnitInterface interface
    public:
        void setDeviceTag(const string& deviceName, const string& tag, const Ice::Current&);
        void startLogging(const Ice::Current&);
        void stopLogging(const Ice::Current&);
    };
}

#endif
