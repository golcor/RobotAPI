armarx_set_target("HandUnitGuiPlugin")

find_package(Qt4 COMPONENTS QtCore QtGui QUIET)
armarx_build_if(QT_FOUND "Qt not available")

find_package(Simox ${ArmarX_Simox_VERSION} QUIET)
#find_package(ArmarXGui QUIET)

armarx_build_if(ArmarXGui_FOUND "ArmarXGui not available")
armarx_build_if(Simox_FOUND "Simox not available")


include(${QT_USE_FILE})

file(GLOB SOURCES HandUnitGuiPlugin.cpp
     HandUnitConfigDialog.cpp)
file(GLOB HEADERS HandUnitGuiPlugin.h
     HandUnitConfigDialog.h)

set(GUI_MOC_HDRS
    HandUnitGuiPlugin.h
    HandUnitConfigDialog.h
)

set(GUI_UIS
    HandUnitGuiPlugin.ui
    HandUnitConfigDialog.ui
)

set(COMPONENT_LIBS RobotAPIUnits ArmarXCoreInterfaces ${Simox_LIBRARIES})




if (ArmarXGui_FOUND)
	armarx_gui_library(HandUnitGuiPlugin "${SOURCES}" "${GUI_MOC_HDRS}" "${GUI_UIS}" "" "${COMPONENT_LIBS}" )
endif()
