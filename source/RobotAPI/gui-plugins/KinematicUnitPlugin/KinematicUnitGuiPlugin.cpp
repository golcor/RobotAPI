#include "KinematicUnitGuiPlugin.h"
#include "KinematicUnitConfigDialog.h"

#include <RobotAPI/gui-plugins/KinematicUnitPlugin/ui_KinematicUnitConfigDialog.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/ArmarXManager.h>

#include <VirtualRobot/XML/RobotIO.h>

// Qt headers
#include <Qt>
#include <QtGlobal>
#include <QSpinBox>
#include <QSlider>
#include <QPushButton>
#include <QStringList>
#include <QTableView>
#include <QCheckBox>
#include <QTableWidget>

#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
// System
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <cmath>

#include <boost/filesystem.hpp>


//#define KINEMATIC_UNIT_FILE_DEFAULT std::string("RobotAPI/robots/Armar3/ArmarIII.xml")
//#define KINEMATIC_UNIT_FILE_DEFAULT_PACKAGE std::string("RobotAPI")
#define KINEMATIC_UNIT_NAME_DEFAULT "Robot"
//#define TOPIC_NAME_DEFAULT "RobotState"

using namespace armarx;
using namespace VirtualRobot;
using namespace std;


KinematicUnitGuiPlugin::KinematicUnitGuiPlugin()
{
    addWidget<KinematicUnitWidgetController>();
}

KinematicUnitWidgetController::KinematicUnitWidgetController() :
    kinematicUnitNode(nullptr),
    selectedControlMode(ePositionControl)
{
    rootVisu = NULL;
    debugLayerVisu = NULL;

    // init gui
    ui.setupUi(getWidget());
    getWidget()->setEnabled(false);

    ui.tableJointList->setItemDelegateForColumn(eTabelColumnAngleProgressbar, &delegate);
}

void KinematicUnitWidgetController::onInitComponent()
{
    dirty_squaresum_old.resize(5, 0);
    ARMARX_INFO << flush;
    verbose = true;


    rootVisu = new SoSeparator;
    rootVisu->ref();
    robotVisu = new SoSeparator;
    robotVisu->ref();
    rootVisu->addChild(robotVisu);

    // create the debugdrawer component
    std::string debugDrawerComponentName = "KinemticUnitGUIDebugDrawer_" + getName();
    ARMARX_INFO << "Creating component " << debugDrawerComponentName;
    debugDrawer = Component::create<DebugDrawerComponent>(properties, debugDrawerComponentName);

    if (mutex3D)
    {
        //ARMARX_IMPORTANT << "mutex3d:" << mutex3D.get();
        debugDrawer->setMutex(mutex3D);
    }
    else
    {
        ARMARX_ERROR << " No 3d mutex available...";
    }

    ArmarXManagerPtr m = getArmarXManager();
    m->addObject(debugDrawer, false);


    {
        boost::recursive_mutex::scoped_lock lock(*mutex3D);
        debugLayerVisu = new SoSeparator();
        debugLayerVisu->ref();
        debugLayerVisu->addChild(debugDrawer->getVisualization());
        rootVisu->addChild(debugLayerVisu);
    }

    string widgetLabel = "KinematicUnit: " + kinematicUnitName;
    ui.labelKinematicUnitName->setText(QString(widgetLabel.c_str()));

    connectSlots();

    usingProxy(kinematicUnitName);
}

void KinematicUnitWidgetController::onConnectComponent()
{
    kinematicUnitInterfacePrx = getProxy<KinematicUnitInterfacePrx>(kinematicUnitName);
    topicName = kinematicUnitInterfacePrx->getReportTopicName();

    robotVisu->removeAllChildren();

    robot.reset();

    std::string rfile;
    StringList includePaths;

    // get robot filename
    try
    {

        StringList packages = kinematicUnitInterfacePrx->getArmarXPackages();
        packages.push_back(Application::GetProjectName());
        ARMARX_VERBOSE << "ArmarX packages " << packages;

        for (const std::string & projectName : packages)
        {
            if (projectName.empty())
            {
                continue;
            }

            CMakePackageFinder project(projectName);
            StringList projectIncludePaths;
            auto pathsString = project.getDataDir();
            ARMARX_VERBOSE << "Data paths of ArmarX package " << projectName << ": " << pathsString;
            boost::split(projectIncludePaths,
                         pathsString,
                         boost::is_any_of(";,"),
                         boost::token_compress_on);
            ARMARX_VERBOSE << "Result: Data paths of ArmarX package " << projectName << ": " << projectIncludePaths;
            includePaths.insert(includePaths.end(), projectIncludePaths.begin(), projectIncludePaths.end());
        }

        rfile = kinematicUnitInterfacePrx->getRobotFilename();
        ARMARX_VERBOSE << "Relative robot file " << rfile;
        ArmarXDataPath::getAbsolutePath(rfile, rfile, includePaths);
        ARMARX_VERBOSE << "Absolute robot file " << rfile;

        robotNodeSetName = kinematicUnitInterfacePrx->getRobotNodeSetName();
    }
    catch (...)
    {
        ARMARX_ERROR << "Unable to retrieve robot filename";
    }

    try
    {
        ARMARX_INFO << "Loading robot from file " << rfile;
        robot = loadRobotFile(rfile);
    }
    catch (...)
    {
        ARMARX_ERROR << "Failed to init robot";
    }

    if (!robot || !robot->hasRobotNodeSet(robotNodeSetName))
    {
        getObjectScheduler()->terminate();

        if (getWidget()->parentWidget())
        {
            getWidget()->parentWidget()->close();
        }

        std::cout << "returning" << std::endl;
        return;
    }

    kinematicUnitFile = rfile;
    robotNodeSet = robot->getRobotNodeSet(robotNodeSetName);


    initGUIComboBox(robotNodeSet); // init the pull down menu (QT: ComboBox)
    initGUIJointListTable(robotNodeSet);

    kinematicUnitVisualization = getCoinVisualization(robot);
    kinematicUnitNode = kinematicUnitVisualization->getCoinVisualization();
    robotVisu->addChild(kinematicUnitNode);

    // init control mode map
    try
    {
        reportedJointControlModes = kinematicUnitInterfacePrx->getControlModes();
    }
    catch (...)
    {
    }

    initializeUi();

    usingTopic(topicName);

    enableMainWidgetAsync(true);
}

void KinematicUnitWidgetController::onDisconnectComponent()
{
    enableMainWidgetAsync(false);

    {
        boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
        robot.reset();
        robotNodeSet.reset();
        currentNode.reset();
    }

    {
        boost::recursive_mutex::scoped_lock lock(*mutex3D);
        robotVisu->removeAllChildren();
        debugLayerVisu->removeAllChildren();
    }
}

void KinematicUnitWidgetController::onExitComponent()
{
    enableMainWidgetAsync(false);

    {
        boost::recursive_mutex::scoped_lock lock(*mutex3D);

        if (robotVisu)
        {
            robotVisu->removeAllChildren();
            robotVisu->unref();
            robotVisu = NULL;
        }

        if (debugLayerVisu)
        {
            debugLayerVisu->removeAllChildren();
            debugLayerVisu->unref();
            debugLayerVisu = NULL;
        }

        if (rootVisu)
        {
            rootVisu->removeAllChildren();
            rootVisu->unref();
            rootVisu = NULL;
        }
    }

    /*
        if (debugDrawer && debugDrawer->getObjectScheduler())
        {
            ARMARX_INFO << "Removing DebugDrawer component...";
            debugDrawer->getObjectScheduler()->terminate();
            ARMARX_INFO << "Removing DebugDrawer component...done";
        }
    */
}

QPointer<QDialog> KinematicUnitWidgetController::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new KinematicUnitConfigDialog(parent);
        dialog->setName(dialog->getDefaultName());
    }

    return qobject_cast<KinematicUnitConfigDialog*>(dialog);
}

void KinematicUnitWidgetController::configured()
{
    ARMARX_VERBOSE << "KinematicUnitWidget::configured()";
    kinematicUnitName = dialog->proxyFinder->getSelectedProxyName().toStdString();
}

void KinematicUnitWidgetController::loadSettings(QSettings* settings)
{
    kinematicUnitName = settings->value("kinematicUnitName", KINEMATIC_UNIT_NAME_DEFAULT).toString().toStdString();
}

void KinematicUnitWidgetController::saveSettings(QSettings* settings)
{
    settings->setValue("kinematicUnitName", QString::fromStdString(kinematicUnitName));
}


void KinematicUnitWidgetController::showVisuLayers(bool show)
{
    if (debugDrawer)
    {
        if (show)
        {
            debugDrawer->enableAllLayers();
        }
        else
        {
            debugDrawer->disableAllLayers();
        }
    }
}

void KinematicUnitWidgetController::updateGuiElements()
{
    // modelUpdateCB();
}

void KinematicUnitWidgetController::modelUpdateCB()
{
}

SoNode* KinematicUnitWidgetController::getScene()
{
    return rootVisu;
}

void KinematicUnitWidgetController::connectSlots()
{
    connect(ui.pushButtonKinematicUnitPos1,  SIGNAL(clicked()), this, SLOT(kinematicUnitZeroPosition()));

    connect(ui.nodeListComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(selectJoint(int)));
    connect(ui.horizontalSliderKinematicUnitPos, SIGNAL(valueChanged(int)), this, SLOT(sliderValueChanged(int)));

    connect(ui.horizontalSliderKinematicUnitPos, SIGNAL(sliderReleased()), this, SLOT(resetSliderInVelocityControl()));

    connect(ui.radioButtonPositionControl, SIGNAL(clicked(bool)), this, SLOT(setControlModePosition()));
    connect(ui.radioButtonVelocityControl, SIGNAL(clicked(bool)), this, SLOT(setControlModeVelocity()));
    connect(ui.radioButtonTorqueControl, SIGNAL(clicked(bool)), this, SLOT(setControlModeTorque()));

    connect(ui.showDebugLayer, SIGNAL(toggled(bool)), this, SLOT(showVisuLayers(bool)), Qt::QueuedConnection);

    connect(this, SIGNAL(jointAnglesReported()), this, SLOT(updateJointAnglesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointVelocitiesReported()), this, SLOT(updateJointVelocitiesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointTorquesReported()), this, SLOT(updateJointTorquesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointCurrentsReported()), this, SLOT(updateJointCurrentsTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointControlModesReported()), this, SLOT(updateControlModesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointMotorTemperaturesReported()), this, SLOT(updateJointMotorTemperaturesTable()), Qt::QueuedConnection);
    connect(this, SIGNAL(jointStatusesReported()), this, SLOT(updateJointStatusesTable()), Qt::QueuedConnection);

    connect(ui.tableJointList, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(selectJointFromTableWidget(int, int)), Qt::QueuedConnection);
}

void KinematicUnitWidgetController::initializeUi()
{
    //signal clicked is not emitted if you call setDown(), setChecked() or toggle().
    ui.radioButtonPositionControl->setChecked(true);
    setControlModePosition();
    updateControlModesTable();
}

void KinematicUnitWidgetController::kinematicUnitZeroPosition()
{
    if (!robotNodeSet)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    NameValueMap jointAngles;
    NameControlModeMap jointModes;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        jointModes[rn[i]->getName()] = ePositionControl;
        jointAngles[rn[i]->getName()] = 0.0f;
    }

    try
    {
        kinematicUnitInterfacePrx->switchControlMode(jointModes);
        kinematicUnitInterfacePrx->setJointAngles(jointAngles);
    }
    catch (...)
    {
    }

    //set slider to 0 if position control mode is used
    if (selectedControlMode == ePositionControl)
    {
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
    }
}

void KinematicUnitWidgetController::resetSliderInVelocityControl()
{
    if (selectedControlMode == eVelocityControl)
    {
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
    }
}

void KinematicUnitWidgetController::setControlModePosition()
{
    NameControlModeMap jointModes;
    selectedControlMode = ePositionControl;

    if (currentNode)
    {
        jointModes[currentNode->getName()] = ePositionControl;

        if (kinematicUnitInterfacePrx)
        {
            kinematicUnitInterfacePrx->switchControlMode(jointModes);
        }

        float lo = currentNode->getJointLimitLo() * 180.0 / M_PI;
        float hi = currentNode->getJointLimitHi() * 180.0 / M_PI;

        if (hi - lo <= 0.0f)
        {
            return;
        }

        float pos = currentNode->getJointValue() * 180.0 / M_PI;
        ARMARX_INFO << "setting position control for current Node Name: " << currentNode->getName() << " with current value: " << pos;

        // Setting the slider position to pos will set the position to the slider tick closest to pos
        // This will initially send a position target with a small delta to the joint.
        ui.horizontalSliderKinematicUnitPos->blockSignals(true);

        ui.horizontalSliderKinematicUnitPos->setMaximum(hi);
        ui.horizontalSliderKinematicUnitPos->setMinimum(lo);
        ui.horizontalSliderKinematicUnitPos->setSliderPosition((int)(pos));

        ui.horizontalSliderKinematicUnitPos->blockSignals(false);
    }
}

void KinematicUnitWidgetController::setControlModeVelocity()
{
    NameControlModeMap jointModes;

    if (currentNode)
    {
        jointModes[currentNode->getName()] = eVelocityControl;

        ARMARX_INFO << "setting velocity control for current Node Name: " << currentNode->getName() << flush;
        ui.horizontalSliderKinematicUnitPos->setMaximum(90);
        ui.horizontalSliderKinematicUnitPos->setMinimum(-90);

        try
        {
            if (kinematicUnitInterfacePrx)
            {
                kinematicUnitInterfacePrx->switchControlMode(jointModes);
            }
        }
        catch (...)
        {

        }

        selectedControlMode = eVelocityControl;
    }

    resetSliderInVelocityControl();
}

void KinematicUnitWidgetController::setControlModeTorque()
{
    NameControlModeMap jointModes;

    if (currentNode)
    {
        jointModes[currentNode->getName()] = eTorqueControl;
        ui.horizontalSliderKinematicUnitPos->setMaximum(20.0); // TODO: set useful values!
        ui.horizontalSliderKinematicUnitPos->setMinimum(-20.0);
        ARMARX_INFO << "setting torque control for current Node Name: " << currentNode->getName() << flush;

        if (kinematicUnitInterfacePrx)
        {
            try
            {
                kinematicUnitInterfacePrx->switchControlMode(jointModes);
            }
            catch (...)
            {

            }
        }
    }

    selectedControlMode = eTorqueControl;
}

VirtualRobot::RobotPtr KinematicUnitWidgetController::loadRobotFile(std::string fileName)
{
    VirtualRobot::RobotPtr robot;

    if (verbose)
    {
        ARMARX_INFO << "Loading KinematicUnit " << kinematicUnitName << " from " << kinematicUnitFile << " ..." << flush;
    }

    if (!ArmarXDataPath::getAbsolutePath(fileName, fileName))
    {
        ARMARX_INFO << "Could not find Robot XML file with name " << fileName << flush;
    }

    robot = RobotIO::loadRobot(fileName);

    if (!robot)
    {
        ARMARX_INFO << "Could not find Robot XML file with name " << fileName << "(" << kinematicUnitName << ")" << flush;
    }

    return robot;
}

CoinVisualizationPtr KinematicUnitWidgetController::getCoinVisualization(VirtualRobot::RobotPtr robot)
{
    CoinVisualizationPtr coinVisualization;

    if (robot != NULL)
    {
        ARMARX_INFO << "getting coin visualization" << flush;
        coinVisualization = robot->getVisualization<CoinVisualization>();

        if (!coinVisualization || !coinVisualization->getCoinVisualization())
        {
            ARMARX_INFO << "could not get coin visualization" << flush;
        }
    }

    return coinVisualization;
}

VirtualRobot::RobotNodeSetPtr KinematicUnitWidgetController::getRobotNodeSet(VirtualRobot::RobotPtr robot, std::string nodeSetName)
{
    VirtualRobot::RobotNodeSetPtr nodeSetPtr;

    if (robot)
    {
        nodeSetPtr = robot->getRobotNodeSet(nodeSetName);

        if (!nodeSetPtr)
        {
            ARMARX_INFO << "RobotNodeSet with name " << nodeSetName << " is not defined" << flush;

        }
    }

    return nodeSetPtr;
}


bool KinematicUnitWidgetController::initGUIComboBox(VirtualRobot::RobotNodeSetPtr robotNodeSet)
{
    ui.nodeListComboBox->clear();

    if (robotNodeSet)
    {
        std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();

        for (unsigned int i = 0; i < rn.size(); i++)
        {
            //            ARMARX_INFO << "adding item to joint combo box" << rn[i]->getName() << flush;
            QString name(rn[i]->getName().c_str());
            ui.nodeListComboBox->addItem(name);
        }

        selectJoint(0);
        ui.nodeListComboBox->setCurrentIndex(0);

        return true;
    }

    return false;
}


bool KinematicUnitWidgetController::initGUIJointListTable(VirtualRobot::RobotNodeSetPtr robotNodeSet)
{
    uint numberOfColumns = 11;

    //dont use clear! It is not required here and somehow causes the tabel to have
    //numberOfColumns additional empty columns and rn.size() additional empty rows.
    //Somehow columncount (rowcount) stay at numberOfColumns (rn.size())
    //ui.tableJointList->clear();

    if (robotNodeSet)
    {
        std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();

        //set dimension of table
        //ui.tableJointList->setColumnWidth(0,110);

        //ui.tableJointList->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
        ui.tableJointList->setRowCount(rn.size());
        ui.tableJointList->setColumnCount(numberOfColumns);


        //ui.tableJointList->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);

        // set table header
        // if the order is changed dont forget to update the order in the enum JointTabelColumnIndex
        // in theheader file
        QStringList s;
        s << "Joint Name"
          << "Control Mode"
          << "Angle [rad]"
          << "Velocity [rad/s]"
          << "Torque [Nm]"
          << "Current [A]"
          << "Temperature [C]"
          << "Operation"
          << "Error"
          << "Enabled"
          << "Emergency Stop";
        ui.tableJointList->setHorizontalHeaderLabels(s);



        // fill in joint names
        for (unsigned int i = 0; i < rn.size(); i++)
        {
            //         ARMARX_INFO << "adding item to joint table" << rn[i]->getName() << flush;
            QString name(rn[i]->getName().c_str());

            QTableWidgetItem* newItem = new QTableWidgetItem(name);
            ui.tableJointList->setItem(i, eTabelColumnName, newItem);
        }

        // init missing table fields with default values
        for (unsigned int i = 0; i < rn.size(); i++)
        {
            for (unsigned int j = 1; j < numberOfColumns; j++)
            {
                QString state = "--";
                QTableWidgetItem* newItem = new QTableWidgetItem(state);
                ui.tableJointList->setItem(i, j, newItem);
            }
        }

        //hide columns Operation, Error, Enabled and Emergency Stop
        //they will be shown when changes occur
        // TODO: for some reason the columns are not hidden
        ui.tableJointList->setColumnHidden(eTabelColumnOperation, true);
        ui.tableJointList->setColumnHidden(eTabelColumnError , true);
        ui.tableJointList->setColumnHidden(eTabelColumnEnabled , true);
        ui.tableJointList->setColumnHidden(eTabelColumnEmergencyStop , true);

        return true;
    }

    return false;
}


void KinematicUnitWidgetController::selectJoint(int i)
{
    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);

    if (!robotNodeSet || i < 0 || i >= (int)robotNodeSet->getSize())
    {
        return;
    }

    currentNode = robotNodeSet->getAllRobotNodes()[i];

    /*
    //ui.lcdNumberKinematicUnitJointValue->display(pos*180.0f/(float)M_PI);
    ui.lcdNumberKinematicUnitJointValue->display(posT);
    */
    if (selectedControlMode == ePositionControl)
    {
        setControlModePosition();
    }
    else if (selectedControlMode == eVelocityControl)
    {
        ui.horizontalSliderKinematicUnitPos->setMaximum(60);
        ui.horizontalSliderKinematicUnitPos->setMinimum(-60);

        setControlModeVelocity();
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
    }
    else if (selectedControlMode == eTorqueControl)
    {
        ui.horizontalSliderKinematicUnitPos->setMaximum(60);
        ui.horizontalSliderKinematicUnitPos->setMinimum(-60);
        setControlModeTorque();
        ui.horizontalSliderKinematicUnitPos->setSliderPosition(SLIDER_ZERO_POSITION);
    }
}

void KinematicUnitWidgetController::selectJointFromTableWidget(int row, int column)
{
    if (column == eTabelColumnName)
    {
        ui.nodeListComboBox->setCurrentIndex(row);
        //        selectJoint(row);
    }
}

void KinematicUnitWidgetController::sliderValueChanged(int pos)
{
    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);

    if (!currentNode)
    {
        return;
    }

    //    float ticks = static_cast<float>(ui.horizontalSliderKinematicUnitPos->maximum() - ui.horizontalSliderKinematicUnitPos->minimum()+1);
    float value = static_cast<float>(ui.horizontalSliderKinematicUnitPos->value());
    //    float perc = (value - static_cast<float>(ui.horizontalSliderKinematicUnitPos->minimum())) / ticks;

    //    NameControlModeMap::const_iterator it;
    //    it = reportedJointControlModes.find(currentNode->getName());
    //    const ControlMode currentControlMode = it->second;
    const ControlMode currentControlMode = selectedControlMode;

    if (currentControlMode == ePositionControl)
    {
        // TODO: Joint limits are not respected
        //float lo = currentNode->getJointLimitLo();
        //float hi = currentNode->getJointLimitHi();
        //float result = lo + (hi-lo)*perc;
        //jointAngles[currentNode->getName()] = (perc - 0.5) * 2 * M_PI;

        NameValueMap jointAngles;
        jointAngles[currentNode->getName()] = value / 180.0 * M_PI;

        if (kinematicUnitInterfacePrx)
        {
            try
            {
                kinematicUnitInterfacePrx->setJointAngles(jointAngles);
            }
            catch (...)
            {
            }
            updateModel();
        }
    }
    else if (currentControlMode == eVelocityControl)
    {
        NameValueMap jointVelocities;
        jointVelocities[currentNode->getName()] = value / 180.0 * M_PI;;

        if (kinematicUnitInterfacePrx)
        {
            try
            {
                kinematicUnitInterfacePrx->setJointVelocities(jointVelocities);
            }
            catch (...)
            {
            }
            updateModel();
        }
    }
    else if (currentControlMode == eTorqueControl)
    {
        NameValueMap jointTorques;
        jointTorques[currentNode->getName()] = value / 10.0f;

        if (kinematicUnitInterfacePrx)
        {
            try
            {
                kinematicUnitInterfacePrx->setJointTorques(jointTorques);
            }
            catch (...)
            {
            }
            updateModel();
        }
    }
    else
    {
        ARMARX_INFO << "current ControlModes unknown" << flush;
    }
}



void KinematicUnitWidgetController::updateControlModesTable()
{
    if (!getWidget() || !robotNodeSet)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        NameControlModeMap::const_iterator it;
        it = reportedJointControlModes.find(rn[i]->getName());
        QString state;

        if (it == reportedJointControlModes.end())
        {
            state = "unknown";
        }
        else
        {
            ControlMode currentMode = it->second;


            switch (currentMode)
            {
                    /*case eNoMode:
                        state = "None";
                        break;

                    case eUnknownMode:
                        state = "Unknown";
                        break;
                    */
                case eDisabled:
                    state = "Disabled";
                    break;

                case eUnknown:
                    state = "Unknown";
                    break;

                case ePositionControl:
                    state = "Position";
                    break;

                case eVelocityControl:
                    state = "Velocity";
                    break;

                case eTorqueControl:
                    state = "Torque";
                    break;


                case ePositionVelocityControl:
                    state = "Position + Velocity";
                    break;

                default:
                    //show the value of the mode so it can be implemented
                    state = QString("<nyi Mode: %1>").arg(static_cast<int>(currentMode));
                    break;
            }
        }

        QTableWidgetItem* newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnControlMode, newItem);
    }
}

void KinematicUnitWidgetController::updateJointStatusesTable()
{
    if (!getWidget() || !robotNodeSet)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        NameStatusMap::const_iterator it;
        it = reportedJointStatuses.find(rn[i]->getName());
        JointStatus currentStatus = it->second;

        QString state = translateStatus(currentStatus.operation);
        QTableWidgetItem* newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnOperation, newItem);

        state = translateStatus(currentStatus.error);
        newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnError, newItem);

        state = currentStatus.enabled ? "X" : "-";
        newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnEnabled, newItem);

        state = currentStatus.emergencyStop ? "X" : "-";
        newItem = new QTableWidgetItem(state);
        ui.tableJointList->setItem(i, eTabelColumnEmergencyStop, newItem);
    }

    //show columns

    ui.tableJointList->setColumnHidden(eTabelColumnOperation, false);
    ui.tableJointList->setColumnHidden(eTabelColumnError, false);
    ui.tableJointList->setColumnHidden(eTabelColumnEnabled, false);
    ui.tableJointList->setColumnHidden(eTabelColumnEmergencyStop, false);
}

QString KinematicUnitWidgetController::translateStatus(OperationStatus status)
{
    switch (status)
    {
        case eOffline:
            return "Offline";

        case eOnline:
            return "Online";

        case eInitialized:
            return "Initialized";

        default:
            return "?";
    }
}

QString KinematicUnitWidgetController::translateStatus(ErrorStatus status)
{
    switch (status)
    {
        case eOk:
            return "Ok";

        case eWarning:
            return "Wr";

        case eError:
            return "Er";

        default:
            return "?";
    }
}

void KinematicUnitWidgetController::updateJointAnglesTable()
{
    if (!robotNodeSet)
    {
        return;
    }

    float dirty_squaresum = 0;

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();


    for (unsigned int i = 0; i < rn.size(); i++)
    {
        NameValueMap::const_iterator it;
        RobotNodePtr node = rn[i];
        it = reportedJointAngles.find(node->getName());

        if (it == reportedJointAngles.end())
        {
            continue;
        }

        const float currentValue = it->second;
        dirty_squaresum += currentValue * currentValue;

        QModelIndex index = ui.tableJointList->model()->index(i, eTabelColumnAngleProgressbar);

        ui.tableJointList->model()->setData(index, cutJitter(currentValue), eJointAngleRole);
        ui.tableJointList->model()->setData(index, node->getJointLimitHigh(), eJointHiRole);
        ui.tableJointList->model()->setData(index, node->getJointLimitLow(), eJointLoRole);
    }

    //update only if values changed
    if ((fabs(dirty_squaresum_old[0] - dirty_squaresum)) > 0.0000005)
    {
        updateModel();
        dirty_squaresum_old[0] = dirty_squaresum;
        //        ARMARX_INFO << "update model" << flush;
    }
}

void KinematicUnitWidgetController::updateJointVelocitiesTable()
{
    if (!getWidget() || !robotNodeSet)
    {
        return;
    }

    float dirty_squaresum = 0;
    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    QTableWidgetItem* newItem;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        NameValueMap::const_iterator it;
        it = reportedJointVelocities.find(rn[i]->getName());

        if (it == reportedJointVelocities.end())
        {
            continue;
        }

        const float currentValue = it->second;
        dirty_squaresum += currentValue * currentValue;
        const QString Text = QString::number(cutJitter(currentValue));
        newItem = new QTableWidgetItem(Text);
        ui.tableJointList->setItem(i, eTabelColumnVelocity, newItem);
    }

    if ((fabs(dirty_squaresum_old[1] - dirty_squaresum)) > 0.0000005)
    {
        updateModel();
        dirty_squaresum_old[1] = dirty_squaresum;
    }
}

void KinematicUnitWidgetController::updateJointTorquesTable()
{
    if (!getWidget() || !robotNodeSet)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    QTableWidgetItem* newItem;
    NameValueMap::const_iterator it;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        it = reportedJointTorques.find(rn[i]->getName());

        if (it == reportedJointTorques.end())
        {
            continue;
        }

        const float currentValue = it->second;
        newItem = new QTableWidgetItem(QString::number(cutJitter(currentValue)));
        ui.tableJointList->setItem(i, eTabelColumnTorque, newItem);
    }
}

void KinematicUnitWidgetController::updateJointCurrentsTable()
{
    if (!getWidget() || !robotNodeSet)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    QTableWidgetItem* newItem;
    NameValueMap::const_iterator it;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        it = reportedJointCurrents.find(rn[i]->getName());

        if (it == reportedJointCurrents.end())
        {
            continue;
        }

        const float currentValue = it->second;
        newItem = new QTableWidgetItem(QString::number(cutJitter(currentValue)));
        ui.tableJointList->setItem(i, eTabelColumnCurrent, newItem);
    }
}

void KinematicUnitWidgetController::updateJointMotorTemperaturesTable()
{
    if (!getWidget() || !robotNodeSet)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn = robotNodeSet->getAllRobotNodes();
    QTableWidgetItem* newItem;
    NameValueMap::const_iterator it;

    for (unsigned int i = 0; i < rn.size(); i++)
    {
        it = reportedJointMotorTemperatures.find(rn[i]->getName());

        if (it == reportedJointMotorTemperatures.end())
        {
            continue;
        }

        const float currentValue = it->second;
        newItem = new QTableWidgetItem(QString::number(cutJitter(currentValue)));
        ui.tableJointList->setItem(i, eTabelColumnTemperature, newItem);
    }
}

void KinematicUnitWidgetController::reportJointAngles(const NameValueMap& jointAngles, bool aValueChanged, const Ice::Current& c)
{
    if (!aValueChanged && reportedJointAngles.size() > 0)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    reportedJointAngles = jointAngles;
    emit jointAnglesReported();
}

void KinematicUnitWidgetController::reportJointVelocities(const NameValueMap& jointVelocities, bool aValueChanged, const Ice::Current& c)
{
    if (!aValueChanged && reportedJointVelocities.size() > 0)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    reportedJointVelocities = jointVelocities;
    emit jointVelocitiesReported();
}

void KinematicUnitWidgetController::reportJointTorques(const NameValueMap& jointTorques, bool aValueChanged, const Ice::Current& c)
{
    if (!aValueChanged && reportedJointTorques.size() > 0)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    reportedJointTorques = jointTorques;

    emit jointTorquesReported();
}

void KinematicUnitWidgetController::reportJointAccelerations(const NameValueMap& jointAccelerations, bool aValueChanged, const Ice::Current& c)
{

}

void KinematicUnitWidgetController::reportControlModeChanged(const NameControlModeMap& jointModes, bool aValueChanged, const Ice::Current& c)
{
    //    if(!aValueChanged && reportedJointControlModes.size() > 0)
    //        return;
    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);

    for (auto & e : jointModes)
    {
        //        ARMARX_INFO << "Setting jointMode of joint " << e.first << " to " << e.second;

        reportedJointControlModes[e.first] = e.second;
    }

    emit jointControlModesReported();
}

void KinematicUnitWidgetController::reportJointCurrents(const NameValueMap& jointCurrents, bool aValueChanged, const Ice::Current& c)
{
    if (!aValueChanged && reportedJointCurrents.size() > 0)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    reportedJointCurrents = jointCurrents;

    emit jointCurrentsReported();
}

void KinematicUnitWidgetController::reportJointMotorTemperatures(const NameValueMap& jointMotorTemperatures, bool aValueChanged,  const Ice::Current& c)
{
    if (!aValueChanged && reportedJointMotorTemperatures.size() > 0)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    reportedJointMotorTemperatures = jointMotorTemperatures;

    emit jointMotorTemperaturesReported();
}

void KinematicUnitWidgetController::reportJointStatuses(const NameStatusMap& jointStatuses, bool aValueChanged, const Ice::Current&)
{
    if (!aValueChanged && reportedJointStatuses.size() > 0)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    reportedJointStatuses = jointStatuses;

    emit jointStatusesReported();
}


void KinematicUnitWidgetController::updateModel()
{
    if (!robotNodeSet)
    {
        return;
    }

    //    ARMARX_INFO << "updateModel()" << flush;
    boost::recursive_mutex::scoped_lock lock(mutexNodeSet);
    std::vector< RobotNodePtr > rn2 = robotNodeSet->getAllRobotNodes();

    std::vector< RobotNodePtr > usedNodes;
    std::vector< float > jv;

    for (unsigned int i = 0; i < rn2.size(); i++)
    {
        VirtualRobot::RobotNodePtr node = robot->getRobotNode(rn2[i]->getName());
        NameValueMap::const_iterator it;
        it = reportedJointAngles.find(node->getName());

        if (it != reportedJointAngles.end())
        {
            usedNodes.push_back(node);
            jv.push_back(it->second);
        }
    }

    robot->setJointValues(usedNodes, jv);
}

void KinematicUnitWidgetController::setMutex3D(boost::shared_ptr<boost::recursive_mutex> mutex3D)
{
    //ARMARX_IMPORTANT << "KinematicUnitWidgetController controller " << getInstanceName() << ": set mutex " << mutex3D.get();

    this->mutex3D = mutex3D;

    if (debugDrawer)
    {
        debugDrawer->setMutex(mutex3D);
    }
}

float KinematicUnitWidgetController::cutJitter(float value)
{
    return (abs(value) < static_cast<float>(ui.jitterThresholdSpinBox->value())) ? 0 : value;
}



void RangeValueDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (index.column() == KinematicUnitWidgetController::eTabelColumnAngleProgressbar)
    {
        float jointValue = index.data(KinematicUnitWidgetController::eJointAngleRole).toFloat();
        float loDeg = index.data(KinematicUnitWidgetController::eJointLoRole).toFloat() * 180 / M_PI;
        float hiDeg = index.data(KinematicUnitWidgetController::eJointHiRole).toFloat() * 180 / M_PI;

        if (hiDeg - loDeg <= 0)
        {
            QStyledItemDelegate::paint(painter, option, index);
            return;
        }

        QStyleOptionProgressBarV2 progressBarOption;
        progressBarOption.rect = option.rect;
        progressBarOption.minimum = loDeg;
        progressBarOption.maximum = hiDeg;
        progressBarOption.progress = jointValue * 180 / M_PI;
        progressBarOption.text = QString::number(jointValue);
        progressBarOption.textVisible = true;
        QPalette pal;
        pal.setColor(QPalette::Background, Qt::red);
        progressBarOption.palette = pal;
        QApplication::style()->drawControl(QStyle::CE_ProgressBar,
                                           &progressBarOption, painter);

        //        QProgressBar renderer;
        //        float progressPercentage = (jointValue*180.0f/M_PI-loDeg)/(hiDeg - loDeg);
        //        ARMARX_INFO_S << VAROUT(progressPercentage);
        //        // Customize style using style-sheet..
        //        QColor color((int)(255*progressPercentage), ((int)(255*(1-progressPercentage))), 0);
        //        QString style = renderer.styleSheet();
        //        style += "QProgressBar::chunk { background-color: " + color.name() + "}";
        //        ARMARX_INFO_S << VAROUT(style);
        //        renderer.resize(option.rect.size());
        //        renderer.setMinimum(loDeg);
        //        renderer.setMaximum(hiDeg);
        //        renderer.setValue(jointValue*180.0f);

        //        renderer.setStyleSheet(style);
        //        painter->save();
        //        painter->translate(option.rect.topLeft());
        //        renderer.render(painter);
        //        painter->restore();
    }
    else
    {
        QStyledItemDelegate::paint(painter, option, index);
    }
}

Q_EXPORT_PLUGIN2(robotapi_gui_KinematicUnitGuiPlugin, KinematicUnitGuiPlugin)
