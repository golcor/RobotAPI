#include "RobotViewerGuiPlugin.h"
#include "RobotViewerConfigDialog.h"

#include <RobotAPI/gui-plugins/RobotViewerPlugin/ui_RobotViewerConfigDialog.h>

#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/ArmarXObjectScheduler.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/application/Application.h>

#include <VirtualRobot/XML/RobotIO.h>

// Qt headers
#include <Qt>
#include <QtGlobal>

#include <QPushButton>

#include <QCheckBox>


#include <Inventor/SoDB.h>
#include <Inventor/Qt/SoQt.h>
// System
#include <stdio.h>
#include <string>
#include <string.h>
#include <stdlib.h>
#include <iostream>
#include <cmath>

#include <boost/filesystem.hpp>

#define TIMER_MS 33
#define ROBOTSTATE_NAME_DEFAULT "RobotStateComponent"

using namespace armarx;
using namespace VirtualRobot;
using namespace std;


RobotViewerGuiPlugin::RobotViewerGuiPlugin()
{
    addWidget<RobotViewerWidgetController>();
}


RobotViewerWidgetController::RobotViewerWidgetController()
{
    robotStateComponentName = "";
    rootVisu = NULL;
    robotVisu = NULL;
    debugLayerVisu = NULL;

    timerSensor = NULL;

    // init gui
    ui.setupUi(getWidget());
    getWidget()->setEnabled(false);
}


void RobotViewerWidgetController::onInitComponent()
{
    bool createDebugDrawer = true;
    verbose = true;

    usingProxy(robotStateComponentName);


    rootVisu = new SoSeparator();
    rootVisu->ref();

    robotVisu = new SoSeparator;
    robotVisu->ref();
    rootVisu->addChild(robotVisu);

    // create the debugdrawer component
    if (createDebugDrawer)
    {
        std::string debugDrawerComponentName = "RobotViewerGUIDebugDrawer_" + getName();
        ARMARX_INFO << "Creating component " << debugDrawerComponentName;
        debugDrawer = Component::create<DebugDrawerComponent>(properties, debugDrawerComponentName);

        if (mutex3D)
        {
            debugDrawer->setMutex(mutex3D);
        }
        else
        {
            ARMARX_ERROR << " No 3d mutex available...";
        }

        ArmarXManagerPtr m = getArmarXManager();
        m->addObject(debugDrawer);


        {
            boost::recursive_mutex::scoped_lock lock(*mutex3D);
            debugLayerVisu = new SoSeparator();
            debugLayerVisu->ref();
            debugLayerVisu->addChild(debugDrawer->getVisualization());
            rootVisu->addChild(debugLayerVisu);
        }
    }

    showRoot(true);
}

void RobotViewerWidgetController::onConnectComponent()
{
    robotStateComponentPrx = getProxy<RobotStateComponentInterfacePrx>(robotStateComponentName);

    if (robotVisu)
    {
        robotVisu->removeAllChildren();
    }

    robot.reset();

    std::string rfile;
    StringList includePaths;

    // get robot filename
    try
    {

        StringList packages = robotStateComponentPrx->getArmarXPackages();
        packages.push_back(Application::GetProjectName());
        ARMARX_VERBOSE << "ArmarX packages " << packages;

        for (const std::string& projectName : packages)
        {
            if (projectName.empty())
            {
                continue;
            }

            CMakePackageFinder project(projectName);
            StringList projectIncludePaths;
            auto pathsString = project.getDataDir();
            ARMARX_VERBOSE << "Data paths of ArmarX package " << projectName << ": " << pathsString;
            boost::split(projectIncludePaths,
                         pathsString,
                         boost::is_any_of(";,"),
                         boost::token_compress_on);
            ARMARX_VERBOSE << "Result: Data paths of ArmarX package " << projectName << ": " << projectIncludePaths;
            includePaths.insert(includePaths.end(), projectIncludePaths.begin(), projectIncludePaths.end());

        }

        rfile = robotStateComponentPrx->getRobotFilename();
        ARMARX_VERBOSE << "Relative robot file " << rfile;
        ArmarXDataPath::getAbsolutePath(rfile, rfile, includePaths);
        ARMARX_VERBOSE << "Absolute robot file " << rfile;
    }
    catch (...)
    {
        ARMARX_ERROR << "Unable to retrieve robot filename";
    }

    try
    {
        ARMARX_INFO << "Loading robot from file " << rfile;
        robot = loadRobotFile(rfile);
    }
    catch (...)
    {
        ARMARX_ERROR << "Failed to init robot";
    }

    if (!robot)
    {
        getObjectScheduler()->terminate();

        if (getWidget()->parentWidget())
        {
            getWidget()->parentWidget()->close();
        }

        std::cout << "returning" << std::endl;
        return;
    }

    setRobotVisu(ui.radioButtonCol->isChecked());
    showRobot(ui.cbRobot->isChecked());


    // start update timer
    SoSensorManager* sensor_mgr = SoDB::getSensorManager();
    timerSensor = new SoTimerSensor(timerCB, this);
    timerSensor->setInterval(SbTime(TIMER_MS / 1000.0f));
    sensor_mgr->insertTimerSensor(timerSensor);

    connectSlots();
    enableMainWidgetAsync(true);
}

void RobotViewerWidgetController::onDisconnectComponent()
{

    ARMARX_INFO << "Disconnecting component";

    // stop update timer
    if (timerSensor)
    {
        SoSensorManager* sensor_mgr = SoDB::getSensorManager();
        sensor_mgr->removeTimerSensor(timerSensor);
    }

    ARMARX_INFO << "Disconnecting component: timer stopped";

    robotStateComponentPrx = NULL;
    {
        boost::recursive_mutex::scoped_lock lock(*mutex3D);

        if (robotVisu)
        {
            ARMARX_INFO << "Disconnecting component: removing visu";
            robotVisu->removeAllChildren();
        }

        robot.reset();
    }
    ARMARX_INFO << "Disconnecting component: finished";
}


void RobotViewerWidgetController::onExitComponent()
{
    enableMainWidgetAsync(false);

    {
        boost::recursive_mutex::scoped_lock lock(*mutex3D);

        if (debugLayerVisu)
        {
            debugLayerVisu->removeAllChildren();
            debugLayerVisu->unref();
            debugLayerVisu = NULL;
        }

        if (robotVisu)
        {
            robotVisu->removeAllChildren();
            robotVisu->unref();
            robotVisu = NULL;
        }

        if (rootVisu)
        {
            rootVisu->removeAllChildren();
            rootVisu->unref();
            rootVisu = NULL;
        }
    }

    /*
        if (debugDrawer && debugDrawer->getObjectScheduler())
        {
            ARMARX_INFO << "Removing DebugDrawer component...";
            debugDrawer->getObjectScheduler()->terminate();
            ARMARX_INFO << "Removing DebugDrawer component...done";
        }
    */
}

QPointer<QDialog> RobotViewerWidgetController::getConfigDialog(QWidget* parent)
{
    if (!dialog)
    {
        dialog = new RobotViewerConfigDialog(parent);
        dialog->setName(dialog->getDefaultName());

    }

    return qobject_cast<RobotViewerConfigDialog*>(dialog);
}



void RobotViewerWidgetController::configured()
{
    ARMARX_VERBOSE << "RobotViewerWidget::configured()";
    robotStateComponentName = dialog->proxyFinder->getSelectedProxyName().trimmed().toStdString();
}


void RobotViewerWidgetController::loadSettings(QSettings* settings)
{
    robotStateComponentName = settings->value("RobotStateComponent", QString::fromStdString(ROBOTSTATE_NAME_DEFAULT)).toString().toStdString();
    bool showRob =  settings->value("showRobot", QVariant(true)).toBool();
    bool fullMod =  settings->value("fullModel", QVariant(true)).toBool();
    ui.cbRobot->setChecked(showRob);
    ui.radioButtonFull->setChecked(fullMod);
    ui.radioButtonCol->setChecked(!fullMod);
}

void RobotViewerWidgetController::saveSettings(QSettings* settings)
{
    settings->setValue("RobotStateComponent", QString::fromStdString(robotStateComponentName));
    settings->setValue("showRobot", ui.cbRobot->isChecked());
    settings->setValue("fullModel", ui.radioButtonFull->isChecked());
}

void RobotViewerWidgetController::setRobotVisu(bool colModel)
{
    robotVisu->removeAllChildren();

    if (!robot)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(*mutex3D);

    VirtualRobot::SceneObject::VisualizationType v = VirtualRobot::SceneObject::Full;

    if (colModel)
    {
        v = VirtualRobot::SceneObject::Collision;
    }

    CoinVisualizationPtr robotViewerVisualization = robot->getVisualization<CoinVisualization>(v);

    if (robotViewerVisualization)
    {
        robotVisu->addChild(robotViewerVisualization->getCoinVisualization());
    }
    else
    {
        ARMARX_WARNING << "no robot visu available...";
    }
}

void RobotViewerWidgetController::showVisuLayers(bool show)
{
    if (debugDrawer)
    {
        if (show)
        {
            debugDrawer->enableAllLayers();
        }
        else
        {
            debugDrawer->disableAllLayers();
        }
    }
}

void RobotViewerWidgetController::showRoot(bool show)
{
    if (!debugDrawer)
    {
        return;
    }

    std::string poseName("root");

    if (show)
    {
        Eigen::Matrix4f gp = Eigen::Matrix4f::Identity();
        PosePtr gpP(new Pose(gp));
        debugDrawer->setPoseDebugLayerVisu(poseName, gpP);
    }
    else
    {
        debugDrawer->removePoseDebugLayerVisu(poseName);
    }
}

void RobotViewerWidgetController::showRobot(bool show)
{
    if (!robotVisu)
    {
        return;
    }

    boost::recursive_mutex::scoped_lock lock(*mutex3D);

    if (show && rootVisu->findChild(robotVisu) < 0)
    {
        rootVisu->addChild(robotVisu);
    }
    else if (!show && rootVisu->findChild(robotVisu) >= 0)
    {
        rootVisu->removeChild(robotVisu);
    }
}
SoNode* RobotViewerWidgetController::getScene()
{
    return rootVisu;
}

void RobotViewerWidgetController::timerCB(void* data, SoSensor* sensor)
{
    RobotViewerWidgetController* controller = static_cast<RobotViewerWidgetController*>(data);

    if (!controller)
    {
        return;
    }

    controller->updateRobotVisu();
}


void RobotViewerWidgetController::connectSlots()
{
    connect(this, SIGNAL(robotStatusUpdated()), this, SLOT(updateRobotVisu()));
    connect(ui.cbDebugLayer, SIGNAL(toggled(bool)), this, SLOT(showVisuLayers(bool)), Qt::QueuedConnection);
    connect(ui.cbRoot, SIGNAL(toggled(bool)), this, SLOT(showRoot(bool)), Qt::QueuedConnection);
    connect(ui.cbRobot, SIGNAL(toggled(bool)), this, SLOT(showRobot(bool)), Qt::QueuedConnection);
    connect(ui.radioButtonCol, SIGNAL(toggled(bool)), this, SLOT(colModel(bool)), Qt::QueuedConnection);
    connect(ui.radioButtonFull, SIGNAL(toggled(bool)), this, SLOT(colModel(bool)), Qt::QueuedConnection);
}



VirtualRobot::RobotPtr RobotViewerWidgetController::loadRobotFile(std::string fileName)
{
    VirtualRobot::RobotPtr robot;

    if (!ArmarXDataPath::getAbsolutePath(fileName, fileName))
    {
        ARMARX_INFO << "Could not find Robot XML file with name " << fileName;
    }

    robot = RobotIO::loadRobot(fileName);

    if (!robot)
    {
        ARMARX_INFO << "Could not find Robot XML file with name " << fileName;
    }

    return robot;
}


void RobotViewerWidgetController::colModel(bool c)
{
    bool colModel = false;

    if (ui.radioButtonCol->isChecked())
    {
        colModel = true;
    }

    setRobotVisu(colModel);
}


void RobotViewerWidgetController::updateRobotVisu()
{
    boost::recursive_mutex::scoped_lock lock(*mutex3D);

    if (!robotStateComponentPrx || !robot)
    {
        return;
    }

    try
    {
        RemoteRobot::synchronizeLocalClone(robot, robotStateComponentPrx);
    }
    catch (...)
    {
        ARMARX_INFO << deactivateSpam(5) << "Robot synchronization failed";
        return;
    }

    Eigen::Matrix4f gp = robot->getGlobalPose();
    QString roboInfo("Robot Pose (global): pos: ");
    roboInfo += QString::number(gp(0, 3), 'f', 2);
    roboInfo += QString(", ");
    roboInfo += QString::number(gp(1, 3), 'f', 2);
    roboInfo += QString(", ");
    roboInfo += QString::number(gp(2, 3), 'f', 2);
    roboInfo += QString(", rot:");
    Eigen::Vector3f rpy;
    VirtualRobot::MathTools::eigen4f2rpy(gp, rpy);
    roboInfo += QString::number(rpy(0), 'f', 2);
    roboInfo += QString(", ");
    roboInfo += QString::number(rpy(1), 'f', 2);
    roboInfo += QString(", ");
    roboInfo += QString::number(rpy(2), 'f', 2);
    ui.leRobotInfo->setText(roboInfo);
}

void RobotViewerWidgetController::setMutex3D(boost::shared_ptr<boost::recursive_mutex> mutex3D)
{
    //ARMARX_IMPORTANT << "set mutex3d :" << mutex3D.get();
    this->mutex3D = mutex3D;

    if (debugDrawer)
    {
        debugDrawer->setMutex(mutex3D);
    }
}


Q_EXPORT_PLUGIN2(armarx_gui_RobotViewerGuiPlugin, RobotViewerGuiPlugin)
