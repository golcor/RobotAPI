/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotStateComponent::
 * @author     ( stefan dot ulbrich at kit dot edu)
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_RobotAPI_LinkedPose_H
#define _ARMARX_RobotAPI_LinkedPose_H

#include "FramedPose.h"

#include <RobotAPI/interface/core/LinkedPoseBase.h>
#include <RobotAPI/interface/core/RobotState.h>

#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/observers/variant/Variant.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <sstream>


namespace armarx
{
    namespace VariantType
    {
        // variant types
        const VariantTypeId LinkedPose = Variant::addTypeName("::armarx::LinkedPoseBase");
        const VariantTypeId LinkedDirection = Variant::addTypeName("::armarx::LinkedDirectionBase");
    }


    class LinkedPose;
    typedef IceInternal::Handle<LinkedPose> LinkedPosePtr;

    /**
     * @class LinkedPose
     * @ingroup VariantsGrp
     * @ingroup RobotAPI-FramedPose
     * @brief The LinkedPose class
     */
    class LinkedPose :
        virtual public LinkedPoseBase,
        virtual public FramedPose
    {
    public:
        LinkedPose();
        LinkedPose(const LinkedPose& other);
        LinkedPose(const FramedPose& other, const SharedRobotInterfacePrx& referenceRobot);
        LinkedPose(const Eigen::Matrix3f& m, const Eigen::Vector3f& v, const std::string& frame, const SharedRobotInterfacePrx& referenceRobot);
        LinkedPose(const Eigen::Matrix4f& m, const std::string& frame, const SharedRobotInterfacePrx& referenceRobot);

        virtual ~LinkedPose();

        VirtualRobot::LinkedCoordinate createLinkedCoordinate();


        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const;

        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const;

        std::string output(const Ice::Current& c = ::Ice::Current()) const;

        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const;

        bool validate(const Ice::Current& c = ::Ice::Current());

        void changeFrame(const std::string& newFrame, const Ice::Current& c = ::Ice::Current());
        void changeToGlobal();
        LinkedPosePtr toGlobal() const;

        friend std::ostream& operator<<(std::ostream& stream, const LinkedPose& rhs)
        {
            stream << "LinkedPose: " << std::endl << rhs.output() << std::endl;
            return stream;
        };

        virtual void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());
    protected:

        void ice_postUnmarshal();
    };



    /**
     * @class LinkedDirection is a direction vector (NOT a position vector) with an attached robotstate proxy
     * for frame changes.
     * @ingroup VariantsGrp
     * @ingroup RobotAPI-FramedPose
     * @brief The LinkedDirection class
     */
    class LinkedDirection :
        virtual public LinkedDirectionBase,
        virtual public FramedDirection
    {
    public:
        LinkedDirection();
        LinkedDirection(const LinkedDirection& source);
        LinkedDirection(const Eigen::Vector3f& v, const std::string& frame, const SharedRobotInterfacePrx& referenceRobot);

        virtual ~LinkedDirection();

        void changeFrame(const std::string& newFrame, const Ice::Current& c = Ice::Current());

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const
        {
            return this->clone();
        }

        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const
        {
            return new LinkedDirection(*this);
        }

        std::string output(const Ice::Current& c = ::Ice::Current()) const
        {
            std::stringstream s;
            s << FramedDirection::toEigen() << std::endl << "reference robot: " << referenceRobot;
            return s.str();
        }

        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const
        {
            return VariantType::LinkedDirection;
        }

        bool validate(const Ice::Current& c = ::Ice::Current())
        {
            return true;
        }


        friend std::ostream& operator<<(std::ostream& stream, const LinkedDirection& rhs)
        {
            stream << "LinkedDirection: " << std::endl << rhs.output() << std::endl;
            return stream;
        };

        virtual void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const;
        virtual void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current());

    protected:

        void ice_postUnmarshal();


    };
    typedef IceInternal::Handle<LinkedDirection> LinkedDirectionPtr;
}

#endif
