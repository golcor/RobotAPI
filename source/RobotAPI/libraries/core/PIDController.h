/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::core::PIDController
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotAPI_PIDController_H
#define _ARMARX_COMPONENT_RobotAPI_PIDController_H

#include <ArmarXCore/core/logging/Logging.h>
#include <Eigen/Core>

namespace armarx
{

    class PIDController :
        public Logging
    {
    public:
        PIDController(float Kp, float Ki, float Kd);
        void update(double deltaSec, double measuredValue, double targetValue);
        void update(double measuredValue, double targetValue);
        double getControlValue() const;

        void reset();
        //    protected:
        float Kp, Ki, Kd;
        double integral;
        double derivative;
        double previousError;
        double processValue;
        double target;
        IceUtil::Time lastUpdateTime;
        double controlValue;
        bool firstRun;
        mutable RecursiveMutex mutex;
    };
    typedef boost::shared_ptr<PIDController> PIDControllerPtr;

    class MultiDimPIDController :
        public Logging
    {
    public:
        MultiDimPIDController(float Kp, float Ki, float Kd);
        void update(const double deltaSec, const Eigen::VectorXf& measuredValue, const Eigen::VectorXf& targetValue);
        void update(const Eigen::VectorXf& measuredValue, const Eigen::VectorXf& targetValue);
        const Eigen::VectorXf& getControlValue() const;

        void reset();
        //    protected:
        float Kp, Ki, Kd;
        double integral;
        double derivative;
        double previousError;
        Eigen::VectorXf processValue;
        Eigen::VectorXf target;
        IceUtil::Time lastUpdateTime;
        Eigen::VectorXf controlValue;
        bool firstRun;
        mutable  RecursiveMutex mutex;
    };
    typedef boost::shared_ptr<PIDController> PIDControllerPtr;
}

#endif
