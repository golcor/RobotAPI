#include "ConditionCheckMagnitudeChecks.h"

#include <ArmarXCore/core/util/StringHelpers.h>

namespace armarx
{

    ConditionCheckMagnitudeLarger::ConditionCheckMagnitudeLarger()
    {
        setNumberParameters(1);
        addSupportedType(VariantType::Vector3, createParameterTypeList(1, VariantType::Float));
        addSupportedType(VariantType::FramedDirection, createParameterTypeList(1, VariantType::Float));
        addSupportedType(VariantType::LinkedDirection, createParameterTypeList(1, VariantType::Float));

    }

    ConditionCheck* ConditionCheckMagnitudeLarger::clone()
    {
        return new ConditionCheckMagnitudeLarger(*this);
    }

    bool ConditionCheckMagnitudeLarger::evaluate(const StringVariantMap& dataFields)
    {

        if (dataFields.size() != 1)
        {
            throw InvalidConditionException("Wrong number of datafields for condition magnitude larger: expected 1 actual: " + ValueToString(dataFields.size()));
        }

        const Variant& value = dataFields.begin()->second;
        VariantTypeId type = value.getType();

        if (type == VariantType::Vector3)
        {
            Eigen::Vector3f vec = value.getClass<Vector3>()->toEigen();
            return (vec).norm() > getParameter(0).getFloat();
        }

        if (type == VariantType::FramedDirection)
        {
            FramedDirectionPtr fV1 = value.getClass<FramedDirection>();
            Eigen::Vector3f vec = value.getClass<FramedDirection>()->toEigen();
            return (vec).norm() > getParameter(0).getFloat();
        }

        if (type == VariantType::LinkedDirection)
        {
            LinkedDirectionPtr lV1 = value.getClass<LinkedDirection>();

            Eigen::Vector3f vec = value.getClass<LinkedDirection>()->toEigen();
            return (vec).norm() > getParameter(0).getFloat();
        }

        return false;
    }


    ConditionCheckMagnitudeSmaller::ConditionCheckMagnitudeSmaller()
    {
        setNumberParameters(1);
        addSupportedType(VariantType::Vector3, createParameterTypeList(1, VariantType::Float));
        addSupportedType(VariantType::FramedDirection, createParameterTypeList(1, VariantType::Float));
        addSupportedType(VariantType::LinkedDirection, createParameterTypeList(1, VariantType::Float));

    }

    ConditionCheck* ConditionCheckMagnitudeSmaller::clone()
    {
        return new ConditionCheckMagnitudeSmaller(*this);
    }

    bool ConditionCheckMagnitudeSmaller::evaluate(const StringVariantMap& dataFields)
    {

        if (dataFields.size() != 1)
        {
            ARMARX_WARNING_S << "Size of dataFields: %d\n" << dataFields.size();
            throw InvalidConditionException("Wrong number of datafields for condition equals ");
        }

        const Variant& value = dataFields.begin()->second;
        VariantTypeId type = value.getType();

        if (type == VariantType::Vector3)
        {
            Eigen::Vector3f vec = value.getClass<Vector3>()->toEigen();
            return (vec).norm() < getParameter(0).getFloat();
        }

        if (type == VariantType::FramedDirection)
        {
            //            FramedDirectionPtr fV1 = value.getClass<FramedDirection>();
            Eigen::Vector3f vec = value.getClass<FramedDirection>()->toEigen();
            return (vec).norm() < getParameter(0).getFloat();
        }

        if (type == VariantType::LinkedDirection)
        {
            //            LinkedDirectionPtr lV1 = value.getClass<LinkedDirection>();
            Eigen::Vector3f vec = value.getClass<LinkedDirection>()->toEigen();
            return (vec).norm() < getParameter(0).getFloat();
        }

        return false;
    }

    ConditionCheckMagnitudeInRange::ConditionCheckMagnitudeInRange()
    {
        setNumberParameters(2);
        addSupportedType(VariantType::Vector3, createParameterTypeList(2, VariantType::Float, VariantType::Float));
        addSupportedType(VariantType::FramedDirection, createParameterTypeList(2, VariantType::Float, VariantType::Float));
        addSupportedType(VariantType::LinkedDirection, createParameterTypeList(2, VariantType::Float, VariantType::Float));

    }

    ConditionCheck* ConditionCheckMagnitudeInRange::clone()
    {
        return new ConditionCheckMagnitudeInRange(*this);
    }

    bool ConditionCheckMagnitudeInRange::evaluate(const StringVariantMap& dataFields)
    {
        if (dataFields.size() != 1)
        {
            ARMARX_WARNING_S << "Size of dataFields: " << dataFields.size();
            throw InvalidConditionException("Wrong number of datafields for condition InRange ");
        }

        const Variant& value = dataFields.begin()->second;
        VariantTypeId type = value.getType();

        if (type == VariantType::Vector3)
        {
            Eigen::Vector3f vec = value.getClass<Vector3>()->toEigen();
            return ((vec).norm() > getParameter(0).getFloat()) && ((vec).norm() < getParameter(1).getFloat());
        }

        if (type == VariantType::FramedDirection)
        {
            //            FramedDirectionPtr fV1 = value.getClass<FramedDirection>();
            Eigen::Vector3f vec = value.getClass<FramedDirection>()->toEigen();
            return ((vec).norm() > getParameter(0).getFloat()) && ((vec).norm() < getParameter(1).getFloat());
        }

        if (type == VariantType::LinkedDirection)
        {
            //            LinkedDirectionPtr lV1 = value.getClass<LinkedDirection>();
            Eigen::Vector3f vec = value.getClass<LinkedDirection>()->toEigen();
            return ((vec).norm() > getParameter(0).getFloat()) && ((vec).norm() < getParameter(1).getFloat());
        }

        return false;
    }

}
