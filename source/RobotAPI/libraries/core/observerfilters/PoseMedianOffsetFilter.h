#ifndef _ARMARX_ROBOTAPI_PoseMedianOffsetFilter_H
#define _ARMARX_ROBOTAPI_PoseMedianOffsetFilter_H

#include <RobotAPI/libraries/core/EigenStl.h>
#include <ArmarXCore/observers/filters/MedianFilter.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <RobotAPI/interface/core/PoseBase.h>

namespace armarx
{
    namespace filters
    {

        /**
         * @class PoseMedianOffsetFilter
         * @ingroup ObserverFilters
         * @brief The MedianFilter class provides an implementation
         *  for a median for datafields of type float, int and double.
         */
        class PoseMedianOffsetFilter :
            public ::armarx::PoseMedianOffsetFilterBase,
            public MedianFilter
        {
        public:
            PoseMedianOffsetFilter(int windowSize = 11);

            // DatafieldFilterBase interface
        public:
            VariantBasePtr calculate(const Ice::Current& c) const;

            /**
             * @brief This filter supports: Vector3, FramedDirection, FramedPosition
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current& c) const;

        private:
            Eigen::Vector3f offset;
            Eigen::Vector3f currentValue;
            std::vector<Eigen::Vector3f> data;
            int dataIndex;

            float median(std::vector<float>& values);
            Eigen::Vector3f calculateMedian();

        public:
            void update(Ice::Long timestamp, const VariantBasePtr& value, const Ice::Current& c);

        };

    } // namespace Filters
}

#endif
