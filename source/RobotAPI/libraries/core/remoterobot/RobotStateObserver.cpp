/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Stefan Ulbrich <stefan dot ulbrich at kit dot edu>, Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "RobotStateObserver.h"
#include <RobotAPI/interface/core/RobotState.h>
#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckInRange.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/RobotConfig.h>
#include <VirtualRobot/VirtualRobot.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>

#include <boost/algorithm/string/trim.hpp>

#define TCP_POSE_CHANNEL "TCPPose"
#define TCP_TRANS_VELOCITIES_CHANNEL "TCPVelocities"


using namespace armarx;
using namespace VirtualRobot;

// ********************************************************************
// observer framework hooks
// ********************************************************************
RobotStateObserver::RobotStateObserver()
{

}

void RobotStateObserver::onInitObserver()
{

    // register all checks
    offerConditionCheck("equals", new ConditionCheckEquals());
    offerConditionCheck("inrange", new ConditionCheckInRange());
    offerConditionCheck("larger", new ConditionCheckLarger());
    offerConditionCheck("smaller", new ConditionCheckSmaller());
}

void RobotStateObserver::onConnectObserver()
{


    offerChannel(TCP_POSE_CHANNEL, "TCP poses of the robot.");
    offerChannel(TCP_TRANS_VELOCITIES_CHANNEL, "TCP velocities of the robot.");
}

// ********************************************************************
// private methods
// ********************************************************************




void RobotStateObserver::updatePoses()
{
    if (getState() < eManagedIceObjectStarting)
    {
        return;
    }

    if (!robot)
    {
        return;
    }

    ScopedRecursiveLock lock(dataMutex);
    ReadLockPtr lock2 = robot->getReadLock();
    FramedPoseBaseMap tcpPoses;
    std::string rootFrame =  robot->getRootNode()->getName();

    //IceUtil::Time start = IceUtil::Time::now();
    for (unsigned int i = 0; i < nodesToReport.size(); i++)
    {
        VirtualRobot::RobotNodePtr& node = nodesToReport.at(i);
        const std::string& tcpName  = node->getName();
        const Eigen::Matrix4f& currentPose = node->getPoseInRootFrame();
        tcpPoses[tcpName] = new FramedPose(currentPose, rootFrame, robot->getName());

    }

    udpatePoseDatafields(tcpPoses);

}

void RobotStateObserver::udpatePoseDatafields(const FramedPoseBaseMap& poseMap)
{
    //        ARMARX_INFO << deactivateSpam() << "new tcp poses reported";
    FramedPoseBaseMap::const_iterator it = poseMap.begin();

    for (; it != poseMap.end(); it++)
    {

        FramedPosePtr vec = FramedPosePtr::dynamicCast(it->second);
        const std::string& tcpName = it->first;

        if (!existsDataField(TCP_POSE_CHANNEL, tcpName))
        {
            offerDataFieldWithDefault(TCP_POSE_CHANNEL, tcpName, Variant(it->second), "Pose of " + tcpName);
        }
        else
        {
            setDataField(TCP_POSE_CHANNEL, tcpName, Variant(it->second));
        }

        updateChannel(TCP_POSE_CHANNEL);

        if (!existsChannel(tcpName))
        {
            offerChannel(tcpName, "pose components of " + tcpName);
            offerDataFieldWithDefault(tcpName, "x", Variant(vec->position->x), "X axis");
            offerDataFieldWithDefault(tcpName, "y", Variant(vec->position->y), "Y axis");
            offerDataFieldWithDefault(tcpName, "z", Variant(vec->position->z), "Z axis");
            offerDataFieldWithDefault(tcpName, "qx", Variant(vec->orientation->qx), "Quaternion part x");
            offerDataFieldWithDefault(tcpName, "qy", Variant(vec->orientation->qy), "Quaternion part y");
            offerDataFieldWithDefault(tcpName, "qz", Variant(vec->orientation->qz), "Quaternion part z");
            offerDataFieldWithDefault(tcpName, "qw", Variant(vec->orientation->qw), "Quaternion part w");
            offerDataFieldWithDefault(tcpName, "frame", Variant(vec->frame), "Reference Frame");
        }
        else
        {
            StringVariantBaseMap newValues;
            newValues["x"] =  new Variant(vec->position->x);
            newValues["y"] =  new Variant(vec->position->y);
            newValues["z"] =  new Variant(vec->position->z);
            newValues["qx"] =  new Variant(vec->orientation->qx);
            newValues["qy"] =  new Variant(vec->orientation->qy);
            newValues["qz"] =  new Variant(vec->orientation->qz);
            newValues["qw"] =  new Variant(vec->orientation->qw);
            newValues["frame"] =  new Variant(vec->frame);
            setDataFieldsFlatCopy(tcpName, newValues);
        }

        updateChannel(tcpName);

    }
}

DatafieldRefBasePtr RobotStateObserver::getPoseDatafield(const std::string& nodeName, const Ice::Current&) const
{
    return getDataFieldRef(new DataFieldIdentifier(getName(), TCP_POSE_CHANNEL, nodeName));
}

void RobotStateObserver::updateNodeVelocities(const NameValueMap& jointVel)
{

    if (getState() < eManagedIceObjectStarting)
    {
        return;
    }

    ScopedRecursiveLock lock(dataMutex);

    if (!robot)
    {
        return;
    }

    ReadLockPtr lock2 = robot->getReadLock();

    if (!velocityReportRobot)
    {
        velocityReportRobot = robot->clone(robot->getName());
    }

    //    IceUtil::Time start = IceUtil::Time::now();
    //    ARMARX_INFO << jointVel;    FramedPoseBaseMap tcpPoses;
    FramedDirectionMap tcpTranslationVelocities;
    FramedDirectionMap tcpOrientationVelocities;
    std::string rootFrame =  robot->getRootNode()->getName();
    NameValueMap tempJointAngles = robot->getConfig()->getRobotNodeJointValueMap();
    FramedPoseBaseMap tcpPoses;

    for (unsigned int i = 0; i < nodesToReport.size(); i++)
    {
        RobotNodePtr node = nodesToReport.at(i);
        const std::string& tcpName  = node->getName();
        const Eigen::Matrix4f& currentPose = node->getPoseInRootFrame();
        tcpPoses[tcpName] = new FramedPose(currentPose, rootFrame, robot->getName());

    }

    double tDelta = 0.001;

    for (NameValueMap::iterator it = tempJointAngles.begin(); it != tempJointAngles.end(); it++)
    {
        NameValueMap::const_iterator itSrc = jointVel.find(it->first);

        if (itSrc != jointVel.end())
        {
            it->second += itSrc->second * tDelta;
        }
    }

    velocityReportRobot->setJointValues(tempJointAngles);
    //    ARMARX_INFO << deactivateSpam(1) << "duration: " << (IceUtil::Time::now()-start).toMicroSecondsDouble();
    //    start = IceUtil::Time::now();
    Eigen::Matrix4f mat;
    Eigen::Vector3f rpy;

    for (unsigned int i = 0; i < nodesToReport.size(); i++)
    {
        RobotNodePtr node = velocityReportRobot->getRobotNode(nodesToReport.at(i)->getName());
        const std::string& tcpName  = node->getName();
        const Eigen::Matrix4f& currentPose = node->getPoseInRootFrame();


        FramedPosePtr lastPose = FramedPosePtr::dynamicCast(tcpPoses[tcpName]);

        tcpTranslationVelocities[tcpName] = new FramedDirection((currentPose.block(0, 3, 3, 1) - lastPose->toEigen().block(0, 3, 3, 1)) / tDelta, rootFrame, robot->getName());

        mat = currentPose * lastPose->toEigen().inverse();

        VirtualRobot::MathTools::eigen4f2rpy(mat, rpy);

        tcpOrientationVelocities[tcpName] = new FramedDirection(rpy / tDelta, rootFrame, robot->getName());


    }

    //    ARMARX_INFO << deactivateSpam(1) << "duration2: " << (IceUtil::Time::now()-start).toMicroSecondsDouble();
    //    ARMARX_INFO << deactivateSpam(5) << "TCP Pose Vel Calc took: " << ( IceUtil::Time::now() - start).toMilliSecondsDouble();
    updateVelocityDatafields(tcpTranslationVelocities, tcpOrientationVelocities);
}

void RobotStateObserver::updateVelocityDatafields(const FramedDirectionMap& tcpTranslationVelocities, const FramedDirectionMap& tcpOrientationVelocities)
{
    FramedDirectionMap::const_iterator it = tcpTranslationVelocities.begin();

    for (; it != tcpTranslationVelocities.end(); it++)
    {

        FramedDirectionPtr vec = FramedDirectionPtr::dynamicCast(it->second);
        FramedDirectionPtr vecOri;
        FramedDirectionMap::const_iterator itOri = tcpOrientationVelocities.find(it->first);

        if (itOri != tcpOrientationVelocities.end())
        {
            vecOri = FramedDirectionPtr::dynamicCast(itOri->second);
        }

        const std::string& tcpName = it->first;

        ARMARX_CHECK_EXPRESSION(vec->frame == vecOri->frame);

        if (!existsDataField(TCP_TRANS_VELOCITIES_CHANNEL, tcpName))
        {
            offerDataFieldWithDefault(TCP_TRANS_VELOCITIES_CHANNEL, tcpName, Variant(it->second), "Pose of " + tcpName);
        }
        else
        {
            setDataField(TCP_TRANS_VELOCITIES_CHANNEL, tcpName, Variant(it->second));
        }

        updateChannel(TCP_TRANS_VELOCITIES_CHANNEL);
        const std::string channelName = tcpName + "Velocities";

        if (!existsChannel(channelName))
        {
            offerChannel(channelName, "pose components of " + tcpName);
            offerDataFieldWithDefault(channelName, "x", Variant(vec->x), "X axis");
            offerDataFieldWithDefault(channelName, "y", Variant(vec->y), "Y axis");
            offerDataFieldWithDefault(channelName, "z", Variant(vec->z), "Z axis");
            offerDataFieldWithDefault(channelName, "roll", Variant(vecOri->x), "Roll");
            offerDataFieldWithDefault(channelName, "pitch", Variant(vecOri->y), "Pitch");
            offerDataFieldWithDefault(channelName, "yaw", Variant(vecOri->z), "Yaw");
            offerDataFieldWithDefault(channelName, "frame", Variant(vecOri->frame), "Reference Frame");
        }
        else
        {
            StringVariantBaseMap newValues;
            newValues["x"] =  new Variant(vec->x);
            newValues["y"] =  new Variant(vec->y);
            newValues["z"] =  new Variant(vec->z);
            newValues["roll"] =  new Variant(vecOri->x);
            newValues["pitch"] =  new Variant(vecOri->y);
            newValues["yaw"] =  new Variant(vecOri->z);
            newValues["frame"] =  new Variant(vec->frame);
            setDataFieldsFlatCopy(channelName, newValues);
        }

        updateChannel(channelName);

    }
}


PropertyDefinitionsPtr RobotStateObserver::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new RobotStateObserverPropertyDefinitions(
                                      getConfigIdentifier()));
}

void RobotStateObserver::setRobot(RobotPtr robot)
{
    ScopedRecursiveLock lock(dataMutex);
    this->robot = robot;

    std::vector< VirtualRobot::RobotNodeSetPtr > robotNodes;
    robotNodes = robot->getRobotNodeSets();

    std::string nodesetsString = getProperty<std::string>("TCPsToReport").getValue();

    if (!nodesetsString.empty())
    {
        if (nodesetsString == "*")
        {
            auto nodesets = robot->getRobotNodeSets();

            for (RobotNodeSetPtr& set : nodesets)
            {
                if (set->getTCP())
                {
                    nodesToReport.push_back(set->getTCP());
                }
            }
        }
        else
        {
            std::vector<std::string> nodesetNames;
            boost::split(nodesetNames,
                         nodesetsString,
                         boost::is_any_of(","),
                         boost::token_compress_on);

            for (auto name : nodesetNames)
            {
                boost::trim(name);
                auto node = robot->getRobotNode(name);

                if (node)
                {
                    nodesToReport.push_back(node);
                }
                else
                {
                    ARMARX_ERROR << "Could not find node set with name: " << name;
                }
            }
        }
    }
}
