#ifndef ARMARX_COMPONENT_ARMARX_WEISSHAPTICGROUP_WEISSHAPTICGROUPSTATECHARTCONTEXT_H
#define ARMARX_COMPONENT_ARMARX_WEISSHAPTICGROUP_WEISSHAPTICGROUPSTATECHARTCONTEXT_H

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/statechart/StatechartContext.h>
#include <RobotAPI/interface/units/HapticUnit.h>
#include <RobotAPI/interface/units/WeissHapticUnit.h>

namespace armarx
{
    namespace WeissHapticGroup
    {
        class WeissHapticGroupStatechartContext
            : virtual public StatechartContext
        {
        protected:
            class PropertyDefinitions
                : public StatechartContextPropertyDefinitions
            {
            public:
                PropertyDefinitions(std::string prefix)
                    : StatechartContextPropertyDefinitions(prefix)
                {
                    defineOptionalProperty<std::string>("HapticUnitObserverName", "HapticUnitObserver", "Name of the haptic unit observer that should be used");
                    defineOptionalProperty<std::string>("WeissHapticUnitName", "WeissHapticUnit", "Name of the weiss haptic unit that should be used");
                }
            }; // class PropertyDefinitions

        private:
            HapticUnitObserverInterfacePrx hapticObserver;
            WeissHapticUnitInterfacePrx weissHapticUnit;

        public:
            std::string getDefaultName() const
            {
                return "WeissHapticGroupStatechartContext";
            }
            virtual void onInitStatechartContext()
            {
                usingProxy(getProperty<std::string>("HapticUnitObserverName").getValue());
                usingProxy(getProperty<std::string>("WeissHapticUnitName").getValue());
            }
            virtual void onConnectStatechartContext()
            {
                hapticObserver = getProxy<HapticUnitObserverInterfacePrx>(getProperty<std::string>("HapticUnitObserverName").getValue());
                weissHapticUnit = getProxy<WeissHapticUnitInterfacePrx>(getProperty<std::string>("WeissHapticUnitName").getValue());
            }
            HapticUnitObserverInterfacePrx getHapticObserver() const
            {
                return hapticObserver;
            }
            WeissHapticUnitInterfacePrx getWeissHapticUnit() const
            {
                return weissHapticUnit;
            }
            virtual PropertyDefinitionsPtr createPropertyDefinitions()
            {
                return PropertyDefinitionsPtr(new WeissHapticGroupStatechartContext::PropertyDefinitions(getConfigIdentifier()));
            }
        }; // class WeissHapticGroupStatechartContext
    } // namespace WeissHapticGroup
} // namespace armarx

#endif // ARMARX_COMPONENT_ARMARX_WEISSHAPTICGROUP_WEISSHAPTICGROUPSTATECHARTCONTEXT_H
